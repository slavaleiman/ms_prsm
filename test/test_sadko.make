CLEANUP = rm -f
MKDIR = mkdir -p
TARGET_EXTENSION=

C_COMPILER=gcc

UNITY_ROOT=../../Unity
SRC_DIR=..

CFLAGS=-std=c99
# CFLAGS += -Wall
CFLAGS += -Wextra
# CFLAGS += -Wpointer-arith
CFLAGS += -Wcast-align
CFLAGS += -Wwrite-strings
CFLAGS += -Wswitch-default
CFLAGS += -Wunreachable-code
CFLAGS += -Winit-self
CFLAGS += -Wmissing-field-initializers
CFLAGS += -Wno-unknown-pragmas
CFLAGS += -Wstrict-prototypes
CFLAGS += -Wundef
CFLAGS += -Wold-style-definition -g3
CFLAGS += -m32
# CFLAGS += -Werror

TARGET=test_sadko

SRC_FILES=$(UNITY_ROOT)/src/unity.c \
	$(SRC_DIR)/Src/sadko.c \
	test_sadko.c

INC_DIRS=-I../Src \
	-I$(UNITY_ROOT)/src \
	-I$(SRC_DIR)/ \
	-I$(SRC_DIR)/Src

SYMBOLS=-DSTM32F405 -DUNITTEST=1

LIBS = -lm

all: clean esud

esud: $(SRC_FILES)
	$(C_COMPILER) $(CFLAGS) $(INC_DIRS) $(SYMBOLS) $(SRC_FILES) -o $(TARGET) $(LIBS)
	- ./$(TARGET)

clean:
	$(CLEANUP) $(TARGET)
