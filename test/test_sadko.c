#include <unistd.h>
#include "unity.h"
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "sadko.h"

void setUp(void)
{
}

void tearDown(void)
{

}

bool log_is_init(void)
{
    return true;
}

bool sd_is_init(void)
{
    return true;
}

int sd_mount(void)
{

}

int sd_write_data(char* filename, uint8_t* data, uint16_t len, uint8_t flags)
{

}

int16_t __REVSH(int16_t value)
{
    return value; // здесь другой эндианес и не надо разворачивать !!!!! ПРОВЕРЬ ЭТО!!!!!
}

int32_t __REV16(uint32_t value)
{
    return value;
}

extern sadko_t sadko;

// int sadko_init(usart_port_t* port, uint8_t addr)
int sadko_init(uint8_t addr)
{
    // port read write callbacks
    // sadko.uport = port;
    // sadko.uport->addr = addr;
    // sadko.uport->read_data = sadko_read_input;
    // sadko.uport->write_data = sadko_write_data;
    // sadko.output_id = 0;

    sadko.output_ready = false;  // если true -> можно отправлять сообщение в садок
    sadko.is_file_opened = false;

    return 0;
}

// void test_read_msg(void)
// {
//     char filename[] = "../toSD/.input_config";
//     FILE* fd = fopen(filename, "r");
//     if(!fd) 
//         return 1;
//     // find_ranges(fd, filename);
//     // rewind(fd);
//     // find_types(fd, filename);
// }

int test_endiannes(void)
{
  uint8_t s[8] = {0};
  uint16_t va = 0xAA00;
  memcpy(s, &va, 2);  // 0x00 0xAA 0x00 0x00 - little endian - lsb first
  if(s[0] == 0)
    return 1;
  else
    return 0xB;
}

// 0x00 0x0d 0x00 0x18 0x00 0x30 0x45 0x02 0x38 0x6d 0x00 0x00 0x00 0x00 0x00
// 0x00 0x0d 0x00 0x19 0x00 0x30 0x45 0x03 0x38 0x6d 0x00 0x00 0x00 0x00 0xff 
// 0x00 0x17 0x00 0x1a 0x00 0x30 0x45 0x0b 0x38 0x6d 0x00 0x00 0x00 0x00 0x00
// 0x0a 0x0a 0x0a 0x0a 0x0a 0x0a 0x0a 0x0a 0x0a 0x0a

void test_output_format_process(void)
{
    uint8_t msg_full[] = {0x00, 0x0d, 0x00, 0x18, 0x00, 0x30, 0x45, 0x02, 0x38, 0x6d, 0x00, 0x00, 0x00, 0x00, 0x00};
    uint16_t len = sizeof(msg_full);
    sadko_process_output_format(&msg_full, &len);
}

int main(void)
{
    UNITY_BEGIN();

    // test_endiannes();

    // sadko читает сообщения из карты которую нужно сэмулировать файлом -> туду заглушка
    // RUN_TEST(test_read_msg);

    // управляется мастером по модбасу, нужно проверить что приходящие modbus сообщения управляют отдачей данных
    // RUN_TEST(test_empty_input);
    // возможно будут различия в endiannes - проверь как работает memcpy в тесте и на железе

    RUN_TEST(test_output_format_process);

    return UNITY_END();
}
