
#include <stdint.h>
// #include "stm32f4xx_hal.h"
#include <time.h>
#include <stdio.h>

void updateRTC(time_t now)
{
	// RTC_TimeTypeDef sTime;
	// RTC_DateTypeDef sDate;

	struct tm time_tm;
	time_tm = *(localtime(&now));

	uint8_t Hours = (uint8_t)time_tm.tm_hour;
	uint8_t Minutes = (uint8_t)time_tm.tm_min;
	uint8_t Seconds = (uint8_t)time_tm.tm_sec;
	// if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN) != HAL_OK)
	// {
	// 	// _Error_Handler(__FILE__, __LINE__);
	// }

	if (time_tm.tm_wday == 0) { time_tm.tm_wday = 7; } // the chip goes mon tue wed thu fri sat sun
	uint8_t WeekDay = (uint8_t)time_tm.tm_wday;
	uint8_t Month = (uint8_t)time_tm.tm_mon + 1; //momth 1- This is why date math is frustrating.
	uint8_t Date = (uint8_t)time_tm.tm_mday;
	uint8_t Year = (uint16_t)(time_tm.tm_year + 1900 - 2000); // time.h is years since 1900, chip is years since 2000

	/*
	* update the RTC
	*/
	// if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BIN) != HAL_OK)
	// {
	// 	// _Error_Handler(__FILE__, __LINE__);
	// }

	// HAL_RTCEx_BKUPWrite(&hrtc, RTC_BKP_DR0, 0x32F2); // lock it in with the backup registers
}

void main(void)
{
    volatile uint64_t now = 0x62B5775B; 
    updateRTC(now);
}