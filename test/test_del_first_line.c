#include <string.h>
#include <stdio.h>

int main(void) // удалить верхнее имя файла из списка
{
    #define MAX 256
    FILE *fptr1, *fptr2;
    char fname[] = ".file_list";
    char str[MAX], temp[] = ".temp";
    fptr1 = fopen(fname, "r");
    if (!fptr1) 
    {
        return -1;
    }
    fptr2 = fopen(temp, "w"); // open the temporary file in write mode
    if (!fptr2) 
    {
        fclose(fptr1);
        return -2;
    }

    fgets(str, MAX, fptr1);
    // copy all contents to the temporary file except first line
    while(!feof(fptr1))
    {
        // strcpy(str, "\0");
        fgets(str, MAX, fptr1);
        if(!feof(fptr1)) // 
        {
            fprintf(fptr2, "%s", str);
        }
    }
    fclose(fptr1);
    fclose(fptr2);
    remove(fname);          // remove the original file 
    rename(temp, fname);    // rename the temporary file to original name    
}
