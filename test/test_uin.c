#include <string.h>
#include <stdio.h>
#include "usart_port.h"
#include "uin.h"

uint8_t master_request[] = {0xA, 0x3, 0xA0, 0x0, 0x0, 0x11, 0x8f, 0x7d};
uint8_t slave_response[] = {0xA, 0x3, 0x22, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 
											0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 
											0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
											0, 1, 2, 3, 0xd, 0xd }; // CRC - просто так

// fill port input buffer
usart_port_t port = {0};

void setUp(void)
{
}

void tearDown(void)
{
}

void memdump(uint8_t *data, uint16_t len, uint16_t str_size)
{
	if(!str_size)
		str_size = 8;

    for(uint16_t i = 0; i < len; ++i)
    {
        if(i && !(i % str_size))
        {
            printf("\n");
        }
        if(i < len)
        {
            printf("%02x ", data[i]);
        }
    }
    printf("\n\n");
}

void input_push_data(usart_port_t* port, uint8_t* data, uint16_t len)
{
	// printf("push data: %x len: %d\n", data, len);
	memdump(data, len, 8);
}

void main(void)
{
	uin_init(&port, 0xA);

	// master req
	memcpy(port.rx_buffer, master_request, sizeof(master_request));
	port.rx_size = sizeof(master_request);
	uin_on_message(&port);
	uin_task();

	// uin response
	memcpy(port.rx_buffer, slave_response, sizeof(slave_response));
	port.rx_size = sizeof(slave_response);
	uin_on_message(&port);
	uin_task();
}
