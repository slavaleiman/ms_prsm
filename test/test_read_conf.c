// test_read_conf.c
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct 
{
	int channel;
	int from;
	int to;
}range_t;

void find_ranges(FILE* file)
{

	char line[255];
	do{
		char* msg = fgets((char*)&line, 255, file);
		if(!msg)
			return;
		char pattern[] = "[ranges]";
		char* ranges = strstr(msg, pattern);
		if(ranges)
		{
			unsigned i = 0;
			do{
				msg = fgets((char*)&line, 255, file); // проверь что чтение пошло дальше
				if(!msg)
					break;
				char* range = strstr(msg, "(");
				if(!range)
					continue;

				int channel = atoi((char*)&range[1]);
				char* echan = strstr(msg, ":");
				if(!echan)
					continue;

				int from = atoi((char*)&echan[1]);

				char* _to = strstr(msg, ",");
				if(!_to)
					continue;
				int to = atoi((char*)&_to[1]);

				range_t r = {channel,from, to};
				++i;
				printf("range %d: (%d : %d, %d)\r\n", i, r.channel, r.from, r.to);
				if(i > 254)
					return;
				// list_insert_tail(input.ranges, &r);
			}while(1);
		}
	}while(1);
}

void find_types(FILE* file)
{

	char line[255];
	do{
		char* msg = fgets((char*)&line, 255, file);
		if(!msg)
			return;
		char pattern[] = "[types]";
		char* ranges = strstr(msg, pattern);
		if(ranges)
		{
			unsigned i = 0;
			do{
				msg = fgets((char*)&line, 255, file); // проверь что чтение пошло дальше
				if(!msg)
					break;
				char* range = strstr(msg, "(");
				if(!range)
					continue;
				int chan = atoi((char*)&range[1]);

				char* type_ = strstr(msg, ":");
				if(!type_)
					continue;
				int type = atoi((char*)&type_[1]);

				// range_t r = {chan, type};
				++i;
				printf("type %d: (%d,%d)\r\n", i, chan, type);
				if(i > 254)
					return;
				// list_insert_tail(input.ranges, &r);
			}while(1);
		}
	}while(1);
}

void find_welding_coef(FILE* file)
{
	char line[255];
	int32_t coeffs[3];
	do{
		char* msg = fgets((char*)&line, 255, file);
		if(!msg)
			return;
		char pattern[] = "[welding_coeff]";
		char* coeffs_pattern = strstr(msg, pattern);
		if(coeffs_pattern)
		{
			unsigned i = 0;
			do{
				++i;
				msg = fgets((char*)&line, 255, file); // проверь что чтение пошло дальше
				if(!msg)
					break;
				coeffs[0] = atoi((char*)msg);
				
				char* sec_substr = strstr(msg, ",");
				if(!sec_substr)
					continue;
				coeffs[1] = atoi((char*)&sec_substr[1]);

				char* thrird_substr = strstr(&sec_substr[1], ",");
				if(!thrird_substr)
					continue;
				coeffs[2] = atoi((char*)&thrird_substr[1]);

				printf("coeffs %d %d %d\r\n", coeffs[0], coeffs[1], coeffs[2]);
				if(coeffs[0] && coeffs[1] && coeffs[2])
					return;
			}while(1);
		}
	}while(1);
}

int main(void)
{
	char filename[] = "../toSD/.input_config";
    FILE* fd = fopen(filename, "r");
	if(!fd) 
		return 1;
	find_ranges(fd);
	rewind(fd);
	find_types(fd);
	rewind(fd);

	find_welding_coef(fd);

	return 0;
}
