#include "input_selector.h"
#include "stm32f4xx_hal.h"

struct {
	uint8_t is_init;
}selector;

void selector_init(void)
{
	// NOW INITED IN discretes_init()

	// GPIO_InitTypeDef GPIO_InitStruct = {0};
	// HAL_GPIO_WritePin(SELECTOR_PORT, SELECTOR_PIN, GPIO_PIN_RESET);
	// GPIO_InitStruct.Pin = SELECTOR_PIN;
	// GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	// GPIO_InitStruct.Pull = GPIO_NOPULL;
	// GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	// HAL_GPIO_Init(SELECTOR_PORT, &GPIO_InitStruct);
}

int selector_current_input(void)
{
	if(!selector.is_init)
	{
		selector_init();
		selector.is_init = 1;
	}
	if(HAL_GPIO_ReadPin(SELECTOR_PORT, SELECTOR_PIN))
		return 1;
	return 0;
}
