#ifndef __USART_PORT__
#define __USART_PORT__ 1

#include <string.h>
#include <stdbool.h>
#include "circular_buffer.h"
#ifndef UNITTEST
#include "stm32f4xx.h"
#endif
#include "soft_timer.h"

#define DMA_RX_BUFFER_SIZE          256
#define PORT_BUFFER_SIZE           	256

typedef struct usart_port_s usart_port_t;

struct usart_port_s
{
#ifndef	UNITTEST
	USART_TypeDef*		usart;
	DMA_Stream_TypeDef*	dma_input_stream;
	uint32_t 			dma_input_stream_channel;
	DMA_Stream_TypeDef* dma_output_stream;
	uint32_t 			dma_output_stream_channel;
	GPIO_TypeDef*		de_port;
	uint32_t			de_pin;
    uint32_t 			baudrate;
    // osSemaphoreId    rx_semaphore;
#endif
    bool 				is_sending;
	uint8_t 			dma_rx_buffer[DMA_RX_BUFFER_SIZE];
    uint8_t     		rx_buffer[PORT_BUFFER_SIZE];
    uint16_t     		rx_size;
    uint8_t     		tx_buffer[PORT_BUFFER_SIZE];
    uint16_t     		tx_size;

    uint16_t            addr;
    bool                is_init;

    void (*on_message)(usart_port_t* port);
    void (*driver_enable)(usart_port_t*, uint8_t);

    int (*read_data)(uint16_t *buffer, uint16_t from_addr, uint8_t num_regs);
    int (*write_data)(uint16_t *buffer, uint16_t from_addr, uint8_t num_regs);

    soft_timer_t* timer;
    uint32_t msg_count;
};

int uport_process(usart_port_t* uport);
#ifndef UNITTEST
void uport_init(    usart_port_t* uport,
                    USART_TypeDef *USART,
                    DMA_Stream_TypeDef *dma_input_stream,
                    uint32_t dma_input_stream_channel,
                    DMA_Stream_TypeDef *dma_output_stream,
                    uint32_t dma_output_stream_channel,
                    uint32_t baudrate,
                    void (*on_message)(usart_port_t* port)
                    );
void usart_on_dma_interrupt(DMA_Stream_TypeDef* dma_stream);
#endif
void uport_transmit(usart_port_t* uport);
void usart_irq_handler(usart_port_t* uport);

#endif
