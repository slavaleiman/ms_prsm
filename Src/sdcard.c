#include "main.h"
#include "fatfs.h"
#include "string.h"
#include "stdio.h"
#include "sdcard.h"
#include "input.h"

struct 
{
    FATFS fs;
    bool is_init;
}sdcard;

int sd_write_data(char* filename, uint8_t* data, uint16_t len, uint8_t flags)
{
    FIL file;
    FRESULT res;
    UINT bw;
    if(!flags)
        flags = FA_WRITE;
    do{
        res = f_open(&file, filename, flags);
        if(res != FR_OK) break;

        res = f_write (&file, (uint8_t*)data, len, &bw);
        if(res != FR_OK) break;
    }while(0);

    f_close(&file);
    return (res >= 0) ? 0 : 1;
}

int sd_read_data(char* filename, uint8_t* data, uint16_t len, uint32_t from)
{
    FRESULT res;
    FIL file;
    UINT br;

    do{
        res = f_open(&file, filename, FA_READ | FA_OPEN_ALWAYS);
        if(res != FR_OK) break;
        res = f_lseek (&file, from);                             /* Move file pointer of the file object */
        if(res != FR_OK) break;
        f_read(&file, (TCHAR*)data, len, &br);
    }while(0);

    f_close(&file);
    return (res == FR_OK) ? 0 : 1;
}

int sd_read_message(FIL* file, uint32_t read_ptr, uint8_t* data, uint16_t* len)
{
    FRESULT res;
    uint32_t position = read_ptr;
    UINT br;
    if(position)
    {
        res = f_lseek(file, position);
        if(res != FR_OK)
            return -1;
    }
    // read len
    res = f_read(file, (TCHAR*)data, 2, &br);
    if(res != FR_OK)
        return -2;
    if(!br)
        return -4;

    *len = (data[0] << 8) | data[1];
    if((*len == 0) || (*len > 255))
        return -3;

    res = f_read(file, (TCHAR*)&data[2], *len, &br);
    if(res != FR_OK) return -4;

    *len = *len + 2; // длина полного сообщения с заголовком
    return 0;
}

bool sd_is_init(void)
{
    return sdcard.is_init;
}

uint8_t sdcard_status(void)
{
    return disk_status(sdcard.fs.drv);
}

void sd_deinit(void)
{
    sdcard.is_init = false;
    memset(&sdcard.fs, 0, sizeof(FATFS));
}

int sd_mount(void)
{
    FRESULT res = f_mount(&sdcard.fs, "", 1);
    if(res != FR_OK)
    {
        return -1;        
    }
    sdcard.is_init = true;
    return 0;
}
