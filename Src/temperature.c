
#include <stdint.h>
#include <stdbool.h>
#include "stm32f4xx_hal.h"
#include "soft_timer.h"
struct{
	bool is_init;
  uint16_t temp; // * умноженная на 10
	soft_timer_t* timer;
}mcu;

ADC_HandleTypeDef hadc1;

static void MX_ADC1_Init(void)
{
  ADC_ChannelConfTypeDef sConfig = {0};

  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    // Error_Handler();
  }

  sConfig.Channel = ADC_CHANNEL_16;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    // Error_Handler();
  }
}

static void start_adc(void)
{
  mcu.temp = (uint16_t)(((3.3* ADC1->DR) / 4096 - 0.76) / 0.0025 + 25.5);
  ADC1->CR2 |= ADC_CR2_SWSTART;
}

static void init_mcu(void)
{
  __HAL_RCC_ADC1_CLK_ENABLE();
	// TODO init adc
	MX_ADC1_Init();
  ADC1->CR2 |= ADC_CR2_SWSTART;
	ADC1->CR2 |= ADC_CR2_ADON;
	mcu.is_init = true;
}

uint16_t temperature(void)
{
	if(!mcu.is_init)
		init_mcu();

  if(!mcu.timer)
  {
    start_adc();
    mcu.timer = soft_timer(5000, start_adc, false, false); // sending
  }
	return mcu.temp;
}
