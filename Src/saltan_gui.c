#include "ugui.h"
#include "ili9341.h"
#include "uptime.h"

// #include "welding_head.h"
// #include "sadko.h"

// #include "errors.h"
// #include "options.h"
// #include "images.h"
// #include "io_gui.h"
// #include "voltage_gui.h"
// #include "tsensor_gui.h"
// #include "tsensor.h"
// #include "rom_data.h"
// #include "shot.h"
// #include "kbd.h"
// #include "pcf8574.h"
// #include "ds2482_100.h"
// #include "ai_control.h"
// #include "string.h"
// #include "soft_timer.h"

static void window_status_callback(UG_MESSAGE* msg);
// static void window_approve_callback(UG_MESSAGE* msg);
// static void window_voltage_callback(UG_MESSAGE* msg);
// static void window_temp_callback(UG_MESSAGE* msg);
// static void window_iostate_callback(UG_MESSAGE* msg);
// void window_setup_callback(UG_MESSAGE* msg);
// void window_options_callback(UG_MESSAGE* msg);
// void window_tsensor_callback(UG_MESSAGE* msg);
// void window_tsensor_edit_callback(UG_MESSAGE* msg);
// void window_param_callback(UG_MESSAGE* msg);
// void window_errors_callback(UG_MESSAGE* msg);

// void (*approve_cb)(void);
// void (*back_cb)(void);

// #define APPROVE_WND_MAX_OBJECTS 3
// UG_WINDOW window_approve;
// UG_OBJECT obj_buff_wnd_approve[APPROVE_WND_MAX_OBJECTS];
// UG_TEXTBOX textbox_approve;
// UG_BUTTON button_no;
// UG_BUTTON button_yes;

#define STATUS_WND_MAX_OBJECTS 6
UG_WINDOW window_status;
UG_OBJECT obj_buff_wnd_status[STATUS_WND_MAX_OBJECTS];
UG_OBJECT* object_status;
UG_OBJECT* object_time;
// UG_BUTTON button_iostate;
// UG_BUTTON button_voltage;
// UG_BUTTON button_temperature;
// UG_BUTTON button_setup;

// #define IOSTATE_WND_MAX_OBJECTS 5
// UG_WINDOW window_iostate;
// UG_OBJECT obj_buff_wnd_iostate[IOSTATE_WND_MAX_OBJECTS];
// UG_OBJECT* object_iostate;
// UG_BUTTON button_iostate_back;
// UG_BUTTON button_iostate_up;
// UG_BUTTON button_iostate_down;
// UG_BUTTON button_iostate_edit;
// bool is_io_test;

// #define VOLTAGE_WND_MAX_OBJECTS 5
// UG_WINDOW window_voltage;
// UG_OBJECT obj_buff_wnd_voltage[VOLTAGE_WND_MAX_OBJECTS];
// UG_OBJECT* object_voltage;
// UG_BUTTON button_voltage_back; // goto status
// UG_BUTTON button_voltage_up;
// UG_BUTTON button_voltage_down;
// UG_BUTTON button_voltage_edit;

// // temperature
// #define TEMP_WND_MAX_OBJECTS 2
// UG_WINDOW window_temp;
// UG_OBJECT obj_buff_wnd_temp[TEMP_WND_MAX_OBJECTS];
// UG_OBJECT* object_temperature;
// UG_BUTTON button_temp_back; // goto status

// #define SETUP_MAX_OBJECTS 5
// UG_WINDOW window_setup;
// UG_OBJECT obj_buff_wnd_setup[SETUP_MAX_OBJECTS];
// UG_BUTTON button_options; // goto params
// UG_BUTTON button_tsensor; // goto temperature sensors
// UG_BUTTON button_param;   // goto param calibration
// UG_BUTTON button_errors;  // goto errors window
// UG_BUTTON button_setup_back; // goto status window

// #define TSENSOR_MAX_OBJECTS 5
// UG_WINDOW window_tsensor;
// UG_OBJECT obj_buff_wnd_tsensor[TSENSOR_MAX_OBJECTS];
// UG_BUTTON button_tsensor_back;
// UG_BUTTON button_tsensor_up;
// UG_BUTTON button_tsensor_down;
// UG_BUTTON button_tsensor_edit;
// UG_OBJECT* object_tsensors_list; // список с выделением и прокруткой

// #define TSENSOR_EDIT_MAX_OBJECTS 23
// UG_WINDOW window_tsensor_edit;
// UG_OBJECT obj_buff_wnd_tsensor_edit[TSENSOR_EDIT_MAX_OBJECTS];
// UG_BUTTON button_tsensor_edit_back;
// UG_BUTTON button_tsensor_edit_delete;
// UG_BUTTON button_tsensor_edit_onoff;
// UG_BUTTON button_tsensor_edit_find;
// UG_OBJECT* object_tsensor_addr; // номер датчика и редактируемый адрес
// UG_BUTTON button_tsensor_activate_keyboard;
// UG_BUTTON button_tsensor_edit_0;
// UG_BUTTON button_tsensor_edit_1;
// UG_BUTTON button_tsensor_edit_2;
// UG_BUTTON button_tsensor_edit_3;
// UG_BUTTON button_tsensor_edit_4;
// UG_BUTTON button_tsensor_edit_5;
// UG_BUTTON button_tsensor_edit_6;
// UG_BUTTON button_tsensor_edit_7;
// UG_BUTTON button_tsensor_edit_8;
// UG_BUTTON button_tsensor_edit_9;
// UG_BUTTON button_tsensor_edit_a;
// UG_BUTTON button_tsensor_edit_b;
// UG_BUTTON button_tsensor_edit_c;
// UG_BUTTON button_tsensor_edit_d;
// UG_BUTTON button_tsensor_edit_e;
// UG_BUTTON button_tsensor_edit_f;

// // параметры калибровки, параметры аккумуляторов, токи, напряжения
// #define PARAM_MAX_OBJECTS 5
// UG_WINDOW window_param; // param callibration
// UG_OBJECT obj_buff_wnd_param_calib[PARAM_MAX_OBJECTS];
// UG_BUTTON button_param_back; // back to main
// UG_BUTTON button_zero;
// UG_BUTTON button_dac_up;
// UG_BUTTON button_dac_down;
// UG_BUTTON button_dac_edit;
// UG_OBJECT* object_param;

// #define ERRORS_MAX_OBJECTS 5
// UG_WINDOW window_errors;
// UG_OBJECT obj_buff_wnd_errors[ERRORS_MAX_OBJECTS];
// UG_BUTTON button_errors_back; // back to main
// UG_BUTTON button_pgdown;
// UG_BUTTON button_pgup;
// UG_BUTTON button_home;
// UG_OBJECT* object_errors;

UG_GUI gui;

// static void battery_status_colors(battery_status_t status, uint16_t* bcolor, uint16_t* fcolor)
// {
//     switch(status)
//     {
//         case BAT_NOT_CONNECTED:
//         case BAT_SWITCHED_OFF:
//         case BAT_BROKEN:
//             *bcolor = C_RED;
//             *fcolor = C_WHITE;
//             break;
//         case BAT_LOW:
//             *bcolor = C_ORANGE;
//             *fcolor = C_RED;
//             break;
//         case BAT_DISCHARGING:
//             *bcolor = C_WHITE;
//             *fcolor = C_RED;
//             break;
//         case BAT_CHARGING:
//             *bcolor = C_ORANGE;
//             *fcolor = C_BLACK;
//             break;
//         case BAT_MAINTENANCE:
//             *bcolor = C_WHITE;
//             *fcolor = C_BLACK;
//             break;
//         case BAT_CHECK:
//             *bcolor = C_WHITE;
//             *fcolor = C_BLACK;
//             break;
//         default:
//             break;
//     }
// }

// char batt_status_str[45];

// static char* battery_status_string(battery_status_t status)
// {
//     switch(status)
//     {
//         case BAT_NOT_CONNECTED:
//             sprintf(batt_status_str, " Батарея не включилась ");
//             break;
//         case BAT_SWITCHED_OFF:
//             sprintf(batt_status_str, " Батарея отключена ");
//             break;
//         case BAT_BROKEN:
//             sprintf(batt_status_str, " Батарея оборвана ");
//             break;
//         case BAT_LOW:
//             sprintf(batt_status_str, " Батарея разряжена ");
//             break;
//         case BAT_DISCHARGING:
//             sprintf(batt_status_str, " Батарея разряжается ");
//             break;
//         case BAT_CHARGING:
//             sprintf(batt_status_str, " Батарея заряжается ");
//             break;
//         case BAT_MAINTENANCE:
//             sprintf(batt_status_str, " Режим сохранения ");
//             break;
//         case BAT_CHECK:
//             sprintf(batt_status_str, " Проверка батареи ");
//             break;
//         default:
//             break;
//     }
//     return batt_status_str;
// }

// void draw_feeder_status(uint8_t* x, uint8_t* y)
// {
//     uint16_t xe = UG_PutString(*x, *y, "Режим и состояние вводов:");
//     uint8_t prio = shot_feeder_priority();
//     char pm[4];
//     switch(prio)
//     {
//         case 0:
//             ILI9341_FillRectangle(xe + 2, *y, 19, 16, C_DARK_GREEN);
//             UG_SetBackcolor(C_DARK_GREEN);
//             UG_SetForecolor(C_WHITE);
//             UG_PutString(xe + 7, *y + 1, "Р");
//             break;
//         case 1:
//         case 2:
//         case 3:
//             ILI9341_FillRectangle(xe + 2, *y, 19, 16, C_DARK_GREEN);
//             UG_SetBackcolor(C_DARK_GREEN);
//             UG_SetForecolor(C_WHITE);
//             sprintf(pm, "%d", prio);
//             UG_PutString(xe + 7, *y + 1, pm);
//             break;
//         case 4:
//         case 5:
//         case 6:
//             ILI9341_FillRectangle(xe + 2, *y, 19, 16, C_RED);
//             UG_SetBackcolor(C_RED);
//             UG_SetForecolor(C_WHITE);
//             sprintf(pm, "%d", prio);
//             UG_PutString(xe + 7, *y + 1, pm);
//             break;
//         default:
//             break;
//     }

//     *y += 18;
// // draw red or green rectangle with symbol
/*#define DRAW_STATUS(X, Y, FLAG, TEXT)                                               \
    {                                                                               \
        if(FLAG)                                                                    \
        {                                                                           \
            ILI9341_FillRectangle(X, Y, 19, 16, C_RED);                             \
            UG_SetBackcolor(C_RED);                                                 \
            UG_SetForecolor(C_WHITE);                                               \
            UG_PutString(X + 1, Y + 1, TEXT);                                       \
        }else{                                                                      \
            ILI9341_FillRectangle(X, Y, 19, 16, C_DARK_GREEN);                      \
            UG_SetBackcolor(C_DARK_GREEN);                                          \
            UG_SetForecolor(C_WHITE);                                               \
            UG_PutString(X + 1, Y + 1, TEXT);                                       \
        }                                                                           \
        UG_SetBackcolor(UG_WindowGetBackColor());                                   \
        UG_SetForecolor(UG_WindowGetForeColor());                                   \
    }
*/
/*#define DRAW_FEEDER(FEED_NUM, X, Y, IS1, IS2, IS3)  \
    DRAW_STATUS(X,      Y, IS1, "A" #FEED_NUM);     \
    DRAW_STATUS(X + 21, Y, IS2, "U" #FEED_NUM);     \
    DRAW_STATUS(X + 42, Y, IS3, "I" #FEED_NUM);     \

    DRAW_FEEDER(1, *x,       *y, (READ_PIN(PCFIN_FAULT_INPUT_1) == PIN_RESET), (READ_PIN(PCFIN_VOLTAGE_INPUT_1) == PIN_RESET), (READ_PIN(PCFIN_ENABLE_INPUT_1) == PIN_RESET));
    DRAW_FEEDER(2, *x + 70,  *y, (READ_PIN(PCFIN_FAULT_INPUT_2) == PIN_RESET), (READ_PIN(PCFIN_VOLTAGE_INPUT_2) == PIN_RESET), (READ_PIN(PCFIN_ENABLE_INPUT_2) == PIN_RESET));
    DRAW_FEEDER(3, *x + 140, *y, (READ_PIN(PCFIN_FAULT_INPUT_3) == PIN_RESET), (READ_PIN(PCFIN_VOLTAGE_INPUT_3) == PIN_RESET), (READ_PIN(PCFIN_ENABLE_INPUT_3) == PIN_RESET));
}
*/
// void time_to_full_charge(char* str, float capacity)
// {
//     uint32_t seconds = (capacity - ai_full_capacity()) / ai_charge_amperage();
//     uint32_t hours = seconds / 3600;
//     uint32_t minutes = (uint32_t)(seconds - hours * 3600) / 60;
//     sprintf(str, "Время до конца заряда:%02u:%02u", (uint8_t)hours, (uint8_t)minutes);
// }

// void time_to_shutdown(char* str, float capacity)
// {
//     uint32_t seconds = capacity / ai_output_amperage();
//     uint32_t hours = seconds / 3600;
//     uint32_t minutes = (uint32_t)(seconds - hours * 3600) / 60;
//     sprintf(str, "Время до выключения:%02u:%02u", (uint8_t)hours, (uint8_t)minutes);
// }

// void draw_battery_status(uint8_t* x, uint8_t* y)
// {
//     UG_PutString(*x, *y, "Статус батареи");
//     *y += 18;
//     // определить цвет сообщения
//     uint16_t bcolor = C_WHITE;
//     uint16_t fcolor = C_RED;

//     battery_status_t bat_status = shot_battery_status();
//     battery_status_colors(bat_status, &bcolor, &fcolor);
//     UG_SetBackcolor(bcolor);
//     UG_SetForecolor(fcolor);
//     // установить фон
//     char* str_status = battery_status_string(bat_status);

//     uint16_t xb = *x + 30;
//     volatile uint16_t xw = UG_PutString(xb, *y, (char*)str_status);
//     ILI9341_FillRectangle(xw, *y, 32, 14, UG_WindowGetBackColor());

//     // вернуть обратно цвета
//     UG_SetBackcolor(UG_WindowGetBackColor());
//     UG_SetForecolor(UG_WindowGetForeColor());

//     *y += 18;
//     char str[42];
//     if(bat_status > BAT_BROKEN)
//     {
//         float cap = ai_battery_capacity();
//         sprintf(str, "Текущая емкость: %0.2f", cap / 3600);
//         UG_PutString(*x, *y, str);
//         charge_mode_t mode = shot_mode();
//         if((mode == CHARGING) || (mode == CHECK_BATTERY))
//         {
//             time_to_full_charge(str, cap);
//         }else{
//             time_to_shutdown(str, cap);
//         }
//     }
//     *y += 18;
//     UG_PutString(*x, *y, str);
//     *y += 18;
// }

// static void draw_error_indicators(uint8_t* x, uint8_t* y)
// {
/*    #define DRAW_ERROR_INDICATOR(X, Y, W, IS_ERR, IS_WARN, TEXT, DELTA_T)                   \
    {                                                                                       \
        if(IS_ERR)                                                                          \
        {                                                                                   \
            ILI9341_FillRectangle(X, Y, W, 16, C_RED);                                      \
            UG_SetBackcolor(C_RED);                                                         \
            UG_SetForecolor(C_WHITE);                                                       \
            UG_PutString(X + 3 + DELTA_T, Y + 1, TEXT);                                     \
        }else if(IS_WARN){                                                                  \
            ILI9341_FillRectangle(X, Y, W, 16, C_ORANGE);                                   \
            UG_SetBackcolor(C_ORANGE);                                                      \
            UG_SetForecolor(C_WHITE);                                                       \
            UG_PutString(X + 3 + DELTA_T, Y + 1, TEXT);                                     \
        }else{                                                                              \
            ILI9341_FillRectangle(X, Y, W, 16, C_DARK_GREEN);                               \
            UG_SetBackcolor(C_DARK_GREEN);                                                  \
            UG_SetForecolor(C_WHITE);                                                       \
            UG_PutString(X + 3 + DELTA_T, Y + 1, TEXT);                                     \
        }                                                                                   \
        UG_SetBackcolor(UG_WindowGetBackColor());                                           \
        UG_SetForecolor(UG_WindowGetForeColor());                                           \
    }*/
//     UG_PutString(*x, *y, "Термометры");
//     uint8_t offset = 110;
//     DRAW_ERROR_INDICATOR(*x + offset, *y,      16, (bool) ds2482_is_error(), false, "Ш", 0); // связь с драйвером
//     DRAW_ERROR_INDICATOR(*x + offset + 17, *y, 16, (bool)(ds2482_is_error() || tsensor_is_errors()), false, "Д", 0); // связь с датчиками
//     DRAW_ERROR_INDICATOR(*x + offset + 34, *y, 16, (bool)(ds2482_is_error() || tsensor_is_errors() || tsensor_is_overheat()), tsensor_is_overrange(), "Т", 0); // превышение температуры

//     *y += 18;
//     UG_PutString(*x, *y, "Аналог Вход");
//     DRAW_ERROR_INDICATOR(*x + offset,      *y, 16, (bool)ai_status(), false, "Д", 0); // состояние SPI
//     DRAW_ERROR_INDICATOR(*x + offset + 17, *y, 16, (bool)(ai_status() || ai_is_overcharge() || ai_is_lowcharge()), ai_is_lowcharge(), "А", 0); // валидность значений ацп

//     *y += 18;
//     UG_PutString(*x, *y, "Аварии внеш.оборудования");
//     *y += 18;
//     uint8_t i_pos = 0;
//     uint8_t i_inc = 22;
//     DRAW_ERROR_INDICATOR(*x + offset + i_pos, *y, 20, (bool)shot_is_error(CIRCUIT_BREAKERS_U_FLAG), false, "U", 2);
//     i_pos += i_inc;
//     DRAW_ERROR_INDICATOR(*x + offset + i_pos, *y, 20, (bool)shot_is_error(CIRCUIT_BREAKERS_I_FLAG), false, "I", 2);
//     i_pos += i_inc;
//     uint8_t status = shot_control_status();
//     DRAW_ERROR_INDICATOR(*x + offset + i_pos, *y, 20, (bool)((status & INSUL_CONTROL_ENABLED_FLAG) && shot_is_error(INSUL_CONTROL_FLAG)), !(status & INSUL_CONTROL_ENABLED_FLAG), "R", 2);
//     i_pos += i_inc;
//     DRAW_ERROR_INDICATOR(*x + offset + i_pos, *y, 20, (bool)shot_is_error(CHARGER1_FLAG), false, "И1", -1);
//     i_pos += i_inc;
//     DRAW_ERROR_INDICATOR(*x + offset + i_pos, *y, 20, (bool)shot_is_error(CHARGER2_FLAG), false, "И2", -1);
//     i_pos += i_inc;
//     DRAW_ERROR_INDICATOR(*x + offset + i_pos, *y, 20, (bool)shot_is_error(CHARGER3_FLAG), false, "И3", -1);
//     *y += 18;
// }

void draw_saltan_status(uint8_t* x, uint8_t* y)
{
    // uint8_t pcfstatus = pcf_status();

    // draw filename
    // draw current params
    



/*    #define DRAW_PCF_FLAG(X, Y, NUMBER, TEXT)                                               \
    {                                                                                       \
        if(pcfstatus & (1 << NUMBER))                                                       \
        {                                                                                   \
            ILI9341_FillRectangle(X + NUMBER * 17, Y, 16, 16, C_RED);                       \
            UG_SetBackcolor(C_RED);                                                         \
            UG_SetForecolor(C_WHITE);                                                       \
            UG_PutString(X + 3 + NUMBER * 17, Y + 1, TEXT);                                 \
        }else{                                                                              \
            ILI9341_FillRectangle(X + NUMBER * 17, Y, 16, 16, C_DARK_GREEN);                \
            UG_SetBackcolor(C_DARK_GREEN);                                                  \
            UG_SetForecolor(C_WHITE);                                                       \
            UG_PutString(X + 3 + NUMBER * 17, Y + 1, TEXT);                                 \
        }                                                                                   \
        UG_SetBackcolor(UG_WindowGetBackColor());                                           \
        UG_SetForecolor(UG_WindowGetForeColor());                                           \
    }*/

    // uint8_t offset = 110;
    // DRAW_PCF_FLAG(*x + offset, *y, 0, "1");
    // DRAW_PCF_FLAG(*x + offset, *y, 1, "2");
    // DRAW_PCF_FLAG(*x + offset, *y, 2, "3");
    // DRAW_PCF_FLAG(*x + offset, *y, 3, "4");
    // DRAW_PCF_FLAG(*x + offset, *y, 4, "5");
    // UG_PutString(*x, *y, "I/O статус");
    // *y += 18;
}

static void saltan_gui_draw_status(UG_WINDOW* wnd, uint8_t x, uint8_t y)
{
    (void)wnd;
    // если не выбрать шрифт, то текст не отобразится
    UG_FontSelect(&FONT_8X14_RU);
    UG_SetBackcolor(UG_WindowGetBackColor());
    UG_SetForecolor(UG_WindowGetForeColor());
    UG_FontSetHSpace(0);
    
    draw_saltan_status(&x, &y);

    // draw_error_indicators(&x, &y);
    // draw_battery_status(&x, &y);
    // draw_feeder_status(&x, &y);
}

// // состояние датчиков температуры
// void saltan_gui_draw_temperature(UG_WINDOW* wnd, uint8_t x, uint8_t y)
// {
//     (void)wnd;
//     char string[64];
//     y += 70;
//     for(uint8_t col = 0; col < 3; ++col)
//     {
//         const uint8_t row_sz = 6;
//         for(uint8_t row = 0; row < row_sz; ++row)
//         {
//             uint8_t id = col * row_sz + row;
//             if(id < 17)
//             {
//                 uint16_t fcolor = C_WHITE;
//                 uint16_t bcolor = C_DARK_GREEN;
//                 uint16_t t = tsensor_value(id);
//                 uint16_t status = shot_measure_status();

//                 if(status & TEMP_SENSOR_WARNING_FLAG)
//                 {
//                     bcolor = C_ORANGE;
//                     fcolor = C_WHITE;
//                 }
//                 if(status & TEMP_SENSOR_ERROR_FLAG)
//                 {
//                     bcolor = C_RED;
//                     fcolor = C_WHITE;
//                 }
//                 if(0xF000 & t)
//                 {
//                     if(t == TSENSOR_NO_SENSOR)
//                     {
//                         bcolor = UG_WindowGetBackColor();
//                         fcolor = C_BLACK;
//                         sprintf(string, "%2d: Откл ", id + 1);
//                     }
//                     else if(t == (TSENSOR_BUSY | TSENSOT_TIMEOUT))
//                     {
//                         bcolor = C_RED;
//                         fcolor = C_WHITE;
//                         sprintf(string, "%2d:------ ", id + 1);
//                     }else{
//                         bcolor = C_RED;
//                         fcolor = C_WHITE;
//                         sprintf(string, "%2d: Ошибка ", id + 1);
//                     }
//                 }else{
//                     sprintf(string, "%2d: %4.1fC ", id + 1, (float)t / 16);
//                 }
//                 UG_SetBackcolor(bcolor);
//                 UG_SetForecolor(fcolor);
//                 UG_PutString(6 + x + col * 96, y + row * 18, string);
//                 UG_SetBackcolor(UG_WindowGetBackColor());
//                 UG_SetForecolor(UG_WindowGetForeColor());
//             }
//         }
//     }
// }

void saltan_gui_update(void)
{
    object_status->state |= OBJ_STATE_UPDATE;
    // object_iostate->state |= OBJ_STATE_UPDATE;
    // object_param->state |= OBJ_STATE_UPDATE;
    // object_errors->state |= OBJ_STATE_UPDATE;
    object_time->state |= OBJ_STATE_UPDATE;
}

// void saltan_gui_update_temp(void)
// {
//     object_temperature->state |= OBJ_STATE_UPDATE;
// }

// void saltan_gui_update_voltage(void)
// {
//     object_voltage->state |= OBJ_STATE_UPDATE;
// }

// static void draw_param_options(opt_list_t* opt_list, uint16_t x, uint16_t y)
// {
//     char str[28];

//     if(opt_list->old_selection == -1)
//         opt_list->update_view = true;

//     const int8_t selected = opt_list->selected - opt_list->view_index;

//     for(int8_t str_num = 0; str_num < OPTIONS_ITEMS_ON_PAGE; ++str_num)
//     {
//         if(str_num == selected)
//         {
//             UG_FontSelect(&FONT_8X14_RU);
//             UG_SetBackcolor(C_LIGHT_SKY_BLUE);
//             uint8_t id = options_item_id(opt_list, str_num);
//             snprintf(str, 15, "%d:%s: %s   ", id + 1, options_item_name(opt_list, str_num), options_item_value(opt_list, str_num));
//             UG_PutString(x, y, str); // здесь рисуем значение коэффициента
//             UG_SetBackcolor(UG_WindowGetBackColor());
//             // а рядом - рисуем рассчитанное напряжение или ток
//             if(options_item_is_coeff(opt_list, str_num))
//             {
//                 float val = ai_value_on_coeff(id); // умноженное значение в зависимости от opt_id
//                 sprintf(str, "%5.1f %s", val, options_item_units(opt_list, str_num));
//                 UG_PutString(x + 132, y, str);
//             }
//         }
//         else if((str_num == opt_list->old_selection) || opt_list->update_view)
//         {
//             opt_list->old_selection = -1;
//             UG_FontSelect(&FONT_8X14_RU);
//             uint8_t id = options_item_id(opt_list, str_num);
//             snprintf(str, 15, "%d:%s: %s   ", id + 1, options_item_name(opt_list, str_num), options_item_value(opt_list, str_num));
//             UG_PutString(x, y, str); // здесь рисуем значение коэффициента
//             // а рядом - стираем рассчитанное напряжение или ток
//             UG_PutString(x + 132, y, "         ");
//         }
//         y += 20;
//     }
//     opt_list->old_selection = selected;
//     opt_list->update_view = false;
// }

// void saltan_gui_param_draw(UG_WINDOW* wnd, UG_OBJECT* obj)
// {
//     (void)wnd;
//     int16_t x = 0;
//     int16_t y = 0;
//     UG_Object_Coordinates(obj, &x, &y);
//     draw_param_options(ai_opt_list(), x, y); // использована нестандартная отрисовка
//     obj->state &= ~OBJ_STATE_UPDATE;
// }

// void saltan_gui_errors_draw(UG_WINDOW* wnd, UG_OBJECT* obj)
// {
//     (void)wnd;
//     int16_t x = 0;
//     int16_t y = 0;
//     UG_Object_Coordinates(obj, &x, &y);
//     UG_PutString(x, y, errors_page());
//     obj->state &= ~OBJ_STATE_UPDATE;
// }

#include "input.h"
#include "sadko.h"

void saltan_gui_status_draw(UG_WINDOW* wnd, UG_OBJECT* obj)
{
    (void)wnd;
    int16_t x = 0;
    int16_t y = 0;
    char string[256];
    UG_Object_Coordinates(obj, &x, &y);
    input_data(string);
    UG_PutString(x, y, string);

    sadko_data(string);
    UG_PutString(x, y + 80, string);

    saltan_gui_draw_status(wnd, x, y);
    obj->state &= ~OBJ_STATE_UPDATE;
}

void saltan_gui_time_draw(UG_WINDOW* wnd, UG_OBJECT* obj)
{
    (void)wnd;
    char string[64];
    uptime_string(string);    
    int16_t x = 0;
    int16_t y = 0;
    UG_Object_Coordinates(obj, &x, &y);
    UG_PutString(x, y, string);
    obj->state &= ~OBJ_STATE_UPDATE;
}

// void saltan_gui_tsensor_addr_draw(UG_WINDOW* wnd, UG_OBJECT* obj)
// {
//     (void)wnd;
//     int16_t x = 0;
//     int16_t y = 0;
//     // coordinates from panel start
//     UG_Object_Coordinates(obj, &x, &y);
//     const uint8_t selected = tsensor_gui_selected_id();
//     char name[24];
//     sprintf(name, "Датчик %d", selected + 1); // from 1
//     // draw sensor name
//     UG_FontSelect(&FONT_8X14_RU);
//     y += 28;
//     UG_PutString(x, y, name);
//     y += 25;
//     UG_PutString(x, y, (tsensor_is_active(selected)) ? "статус: Вкл " : "статус: Выкл");
//     y += 30;
//     uint8_t* addr = tsensor_addr(selected);
//     char address_string[20];
//     sprintf(address_string, "%02x%02x%02x%02x%02x%02x%02x%02x", addr[0], addr[1], addr[2], addr[3], addr[4], addr[5], addr[6], addr[7]);
//     UG_PutString(x, y, address_string);
//     y += 30;
//     uint16_t t = tsensor_value(selected);
//     if(0xF000 & t)
//     {
//         sprintf(address_string, "нет данных");
//     }else{
//         sprintf(address_string, "t = %4.1f C", (float)t / 16);
//     }
//     UG_PutString(x, y, address_string);
//     obj->state &= ~OBJ_STATE_UPDATE;
// }

// void saltan_gui_tsensors_list_draw(UG_WINDOW* wnd, UG_OBJECT* obj)
// {
//     (void)wnd;
//     int16_t x = 0;
//     int16_t y = 0;
//     // coordinates from panel start
//     UG_Object_Coordinates(obj, &x, &y);
//     y += 14;
//     tsensor_gui_draw(x, y);
//     obj->state &= ~OBJ_STATE_UPDATE;
// }

// static void create_approve_window(void)
// {
//     UG_WindowCreate(&window_approve, obj_buff_wnd_approve, APPROVE_WND_MAX_OBJECTS, window_approve_callback);
//     UG_WindowSetTitleText(&window_approve, (char*)"Подтверждение");
//     UG_WindowSetTitleTextFont(&window_approve, &FONT_8X14_RU);

//     UG_WindowSetXEnd(&window_approve, 320);
//     UG_WindowSetYEnd(&window_approve, 238);

//     uint8_t y = 3;
//     const uint8_t btn_width = 80;
//     const uint8_t btn_height= 50;

//     uint16_t width = UG_WindowGetInnerWidth(&window_approve);
//     uint16_t height = UG_WindowGetInnerHeight(&window_approve);

//     UG_ButtonCreate(&window_approve, &button_no, BTN_ID_BACK, 5, y, btn_width, y + btn_height);
//     UG_ButtonSetFont(&window_approve, BTN_ID_BACK, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_approve, BTN_ID_BACK, (char*)"Нет");
//     y += 159;
//     UG_ButtonCreate(&window_approve, &button_yes, BTN_ID_1, 5, y, btn_width, y + btn_height);
//     UG_ButtonSetFont(&window_approve, BTN_ID_1, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_approve, BTN_ID_1, (char*)"Да");

//     UG_TextboxCreate(&window_approve, &textbox_approve, TXB_ID_1, 100, 80, width - 5, height - 5);
//     UG_TextboxSetAlignment(&window_approve, TXB_ID_1, ALIGN_TOP_LEFT);
//     UG_TextboxSetFont(&window_approve, TXB_ID_1, &FONT_8X14_RU);
//     UG_TextboxSetText(&window_approve, TXB_ID_1, (char*)"Вы уверены?");
// }

static void create_status_window(void)
{
    UG_WindowCreate(&window_status, obj_buff_wnd_status, STATUS_WND_MAX_OBJECTS, window_status_callback);
    UG_WindowSetTitleText(&window_status, (char*)"Главное меню");
    UG_WindowSetTitleTextFont(&window_status, &FONT_8X14_RU);

    UG_WindowSetXEnd(&window_status, 320);
    UG_WindowSetYEnd(&window_status, 238);

    // uint8_t x = 5;
    uint8_t y = 23;
    const uint8_t btn_width = 25;
    // const uint8_t btn_height= 50;

    uint16_t width = UG_WindowGetInnerWidth(&window_status);
    uint16_t height = UG_WindowGetInnerHeight(&window_status);

    // UG_ButtonCreate(&window_status, &button_iostate, BTN_ID_0, x, y, x + btn_width, y + btn_height);
    // UG_ButtonSetFont(&window_status, BTN_ID_0, &FONT_8X14_RU);
    // UG_ButtonSetText(&window_status, BTN_ID_0, (char*)"I/O");
    // y += btn_height + 2;
//     UG_ButtonCreate(&window_status, &button_voltage, BTN_ID_1, x, y, x + btn_width, y + btn_height);
//     UG_ButtonSetFont(&window_status, BTN_ID_1, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_status, BTN_ID_1, (char*)"U");
//     y += btn_height + 2;
//     UG_ButtonCreate(&window_status, &button_temperature, BTN_ID_2, x, y, x + btn_width, y + btn_height);
//     UG_ButtonSetFont(&window_status, BTN_ID_2, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_status, BTN_ID_2, (char*)"Т");
//     y += btn_height + 2;
//     UG_ButtonCreate(&window_status, &button_setup, BTN_ID_3, x, y, x + btn_width, y + btn_height);
//     UG_ButtonSetFont(&window_status, BTN_ID_3, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_status, BTN_ID_3, (char*)"Н");
    y = 25;
    UG_Custom_Object_Create(&window_status, &object_time, OBJ_ID_1, btn_width + 12, y, width - 5, y + 18, saltan_gui_time_draw);
    y += 15;
    UG_Custom_Object_Create(&window_status, &object_status, OBJ_ID_0, btn_width + 12, y, width - 5, height - 5, saltan_gui_status_draw);
}

// static void saltan_gui_iostate_draw(UG_WINDOW* wnd, UG_OBJECT* obj)
// {
//     (void)wnd;
//     int16_t x = 0;
//     int16_t y = 0;
//     UG_Object_Coordinates(obj, &x, &y);
//     io_gui_draw(x, y);
//     obj->state &= ~OBJ_STATE_UPDATE;
// }

// static void create_iostate_window(void)
// {
//     io_gui_init(); // там будет храниться состояние выбранного сенсора
//     UG_WindowCreate(&window_iostate, obj_buff_wnd_iostate, IOSTATE_WND_MAX_OBJECTS, window_iostate_callback);
//     UG_WindowSetTitleText(&window_iostate, (char*)"Состояние I/O");
//     UG_WindowSetTitleTextFont(&window_iostate, &FONT_8X14_RU);

//     UG_WindowSetXEnd(&window_iostate, 320);
//     UG_WindowSetYEnd(&window_iostate, 238);

//     const uint8_t btn_w = 75;
//     const uint8_t btn_h = 50;
//     const uint16_t width = UG_WindowGetInnerWidth(&window_iostate);
//     const uint16_t height = UG_WindowGetInnerHeight(&window_iostate);
//     const uint16_t xb = 5;
//     uint8_t y = 3;
//     UG_ButtonCreate(&window_iostate, &button_iostate_back, BTN_ID_BACK, xb, y, xb + btn_w, y + btn_h);
//     UG_ButtonSetFont(&window_iostate, BTN_ID_BACK, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_iostate, BTN_ID_BACK, (char*)"Назад");
//     y += btn_h + 2;
//     UG_ButtonCreate(&window_iostate, &button_iostate_up, BTN_ID_0, xb, y, xb + btn_w, y + btn_h);
//     UG_ButtonSetFont(&window_iostate, BTN_ID_0, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_iostate, BTN_ID_0, (char*)"Пред.");
//     y += btn_h + 2;
//     UG_ButtonCreate(&window_iostate, &button_iostate_down, BTN_ID_1, xb, y, xb + btn_w, y + btn_h);
//     UG_ButtonSetFont(&window_iostate, BTN_ID_1, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_iostate, BTN_ID_1, (char*)"След.");
//     y += btn_h + 2;
//     UG_ButtonCreate(&window_iostate, &button_iostate_edit, BTN_ID_2, xb, y, xb + btn_w, y + btn_h);
//     UG_ButtonSetFont(&window_iostate, BTN_ID_2, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_iostate, BTN_ID_2, (char*)"Править");

//     y = 10;
//     UG_Custom_Object_Create(&window_iostate, &object_iostate, OBJ_ID_0, btn_w + 12, y + 20, width - 5, height - 5, saltan_gui_iostate_draw);
// }

// static void saltan_gui_voltage_draw(UG_WINDOW* wnd, UG_OBJECT* obj)
// {
//     (void)wnd;
//     int16_t x = 0;
//     int16_t y = 0;
//     UG_Object_Coordinates(obj, &x, &y);
//     voltage_gui_draw(x, y);
//     obj->state &= ~OBJ_STATE_UPDATE;
// }

// static void create_voltage_window(void)
// {
//     voltage_gui_init();
//     UG_WindowCreate(&window_voltage, obj_buff_wnd_voltage, VOLTAGE_WND_MAX_OBJECTS, window_voltage_callback);
//     UG_WindowSetTitleText(&window_voltage, (char*)"Напряжение");
//     UG_WindowSetTitleTextFont(&window_voltage, &FONT_8X14_RU);

//     UG_WindowSetXEnd(&window_voltage, 320);
//     UG_WindowSetYEnd(&window_voltage, 238);

//     const uint8_t btn_w = 75;
//     const uint8_t btn_h = 50;
//     const uint16_t width = UG_WindowGetInnerWidth(&window_voltage);
//     const uint16_t height = UG_WindowGetInnerHeight(&window_voltage);
//     const uint16_t xb = 5;
//     uint8_t y = 3;
//     UG_ButtonCreate(&window_voltage, &button_iostate_back, BTN_ID_BACK, xb, y, xb + btn_w, y + btn_h);
//     UG_ButtonSetFont(&window_voltage, BTN_ID_BACK, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_voltage, BTN_ID_BACK, (char*)"Назад");
//     y += btn_h + 2;
//     UG_ButtonCreate(&window_voltage, &button_iostate_up, BTN_ID_0, xb, y, xb + btn_w, y + btn_h);
//     UG_ButtonSetFont(&window_voltage, BTN_ID_0, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_voltage, BTN_ID_0, (char*)"Пред.");
//     y += btn_h + 2;
//     UG_ButtonCreate(&window_voltage, &button_iostate_down, BTN_ID_1, xb, y, xb + btn_w, y + btn_h);
//     UG_ButtonSetFont(&window_voltage, BTN_ID_1, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_voltage, BTN_ID_1, (char*)"След.");
//     y += btn_h + 2;
//     UG_ButtonCreate(&window_voltage, &button_iostate_edit, BTN_ID_2, xb, y, xb + btn_w, y + btn_h);
//     UG_ButtonSetFont(&window_voltage, BTN_ID_2, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_voltage, BTN_ID_2, (char*)"Вкл/\nвыкл");

//     y = 10;
//     UG_Custom_Object_Create(&window_voltage, &object_voltage, OBJ_ID_0, btn_w + 12, y + 20, width - 5, height - 5, saltan_gui_voltage_draw);
// }

// static void saltan_gui_temp_draw(UG_WINDOW* wnd, UG_OBJECT* obj)
// {
//     int16_t x = 0;
//     int16_t y = 0;
//     UG_Object_Coordinates(obj, &x, &y);
//     saltan_gui_draw_temperature(wnd, x, y);
//     obj->state &= ~OBJ_STATE_UPDATE;
// }

// static void create_temperature_window(void)
// {
//     UG_WindowCreate(&window_temp, obj_buff_wnd_temp, TEMP_WND_MAX_OBJECTS, window_temp_callback);
//     UG_WindowSetTitleText(&window_temp, (char*)"Температура");
//     UG_WindowSetTitleTextFont(&window_temp, &FONT_8X14_RU);

//     UG_WindowSetXEnd(&window_temp, 320);
//     UG_WindowSetYEnd(&window_temp, 238);

//     const uint16_t btn_width = 70;
//     const uint8_t btn_height = 50;
//     uint16_t width = UG_WindowGetInnerWidth(&window_temp);
//     uint16_t x = 5;
//     uint16_t xe = width - 5;

//     uint8_t ys = 28; // from screen 0
//     uint8_t ye = UG_WindowGetInnerHeight(&window_temp) - 5;

//     UG_Custom_Object_Create(&window_temp, &object_temperature, OBJ_ID_0, 20, ys, xe, ye, saltan_gui_temp_draw);

//     ys = 3; // from window 0
//     UG_ButtonCreate(&window_temp, &button_temp_back, BTN_ID_BACK, x, ys, btn_width + 5, ys + btn_height);
//     UG_ButtonSetFont(&window_temp, BTN_ID_BACK, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_temp, BTN_ID_BACK, (char*)"Назад");
// }

// static void create_setup_window(void)
// {
//     UG_WindowCreate(&window_setup, obj_buff_wnd_setup, SETUP_MAX_OBJECTS, window_setup_callback);
//     UG_WindowSetTitleText(&window_setup, (char*)"Настройки");
//     UG_WindowSetTitleTextFont(&window_setup, &FONT_8X14_RU);

//     UG_WindowSetXEnd(&window_setup, 320);
//     UG_WindowSetYEnd(&window_setup, 238);

//     uint8_t x = 5;
//     uint8_t y = 3;
//     const uint16_t  btn_width = 301;
//     const uint8_t   btn_height = 50;

//     UG_ButtonCreate(&window_setup, &button_setup_back, BTN_ID_BACK, x, y, x + btn_width, y + btn_height);
//     UG_ButtonSetFont(&window_setup, BTN_ID_BACK, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_setup, BTN_ID_BACK, (char*)"Назад");
//     y += btn_height + 2;
//     UG_ButtonCreate(&window_setup, &button_tsensor, BTN_ID_0, x, y, x + btn_width, y + btn_height);
//     UG_ButtonSetFont(&window_setup, BTN_ID_0, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_setup, BTN_ID_0, (char*)"Термометры");
//     y += btn_height + 2;
//     UG_ButtonCreate(&window_setup, &button_param, BTN_ID_1, x, y, x + btn_width, y + btn_height);
//     UG_ButtonSetFont(&window_setup, BTN_ID_1, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_setup, BTN_ID_1, (char*)"Переменные");
//     y += btn_height + 2;
//     UG_ButtonCreate(&window_setup, &button_errors, BTN_ID_2, x, y, x + btn_width, y + btn_height);
//     UG_ButtonSetFont(&window_setup, BTN_ID_2, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_setup, BTN_ID_2, (char*)"Ошибки");
// }

// static void create_tsensor_window(void)
// {
//     tsensor_gui_init(); // там будет храниться состояние выбранного сенсора
//     UG_WindowCreate(&window_tsensor, obj_buff_wnd_tsensor, TSENSOR_MAX_OBJECTS, window_tsensor_callback);
//     UG_WindowSetTitleText(&window_tsensor, (char*)"Термометры");
//     UG_WindowSetTitleTextFont(&window_tsensor, &FONT_8X14_RU);

//     UG_WindowSetXEnd(&window_tsensor, 320);
//     UG_WindowSetYEnd(&window_tsensor, 238);

//     const uint8_t btn_w = 75;
//     const uint8_t btn_h = 50;
//     const uint16_t width = UG_WindowGetInnerWidth(&window_tsensor);
//     const uint16_t height = UG_WindowGetInnerHeight(&window_tsensor);
//     const uint16_t xb = 5;
//     uint8_t y = 3;
//     UG_ButtonCreate(&window_tsensor, &button_tsensor_back, BTN_ID_BACK, xb, y, xb + btn_w, y + btn_h);
//     UG_ButtonSetFont(&window_tsensor, BTN_ID_BACK, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_tsensor, BTN_ID_BACK, (char*)"Назад");
//     y += btn_h + 2;
//     UG_ButtonCreate(&window_tsensor, &button_tsensor_up, BTN_ID_0, xb, y, xb + btn_w, y + btn_h);
//     UG_ButtonSetFont(&window_tsensor, BTN_ID_0, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_tsensor, BTN_ID_0, (char*)"Вверх");
//     y += btn_h + 2;
//     UG_ButtonCreate(&window_tsensor, &button_tsensor_down, BTN_ID_1, xb, y, xb + btn_w, y + btn_h);
//     UG_ButtonSetFont(&window_tsensor, BTN_ID_1, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_tsensor, BTN_ID_1, (char*)"Вниз");
//     y += btn_h + 2;
//     UG_ButtonCreate(&window_tsensor, &button_tsensor_edit, BTN_ID_2, xb, y, xb + btn_w, y + btn_h);
//     UG_ButtonSetFont(&window_tsensor, BTN_ID_2, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_tsensor, BTN_ID_2, (char*)"Править");

//     y = 10;
//     UG_Custom_Object_Create(&window_tsensor, &object_tsensors_list, OBJ_ID_0, btn_w + 12, y, width - 5, height - 5, saltan_gui_tsensors_list_draw);
// }

// static void create_tsensor_edit_window(void)
// {
//     UG_WindowCreate(&window_tsensor_edit, obj_buff_wnd_tsensor_edit, TSENSOR_EDIT_MAX_OBJECTS, window_tsensor_edit_callback);
//     UG_WindowSetTitleText(&window_tsensor_edit, (char*)"Настройка термометров");
//     UG_WindowSetTitleTextFont(&window_tsensor_edit, &FONT_8X14_RU);

//     UG_WindowSetXEnd(&window_tsensor_edit, 320);
//     UG_WindowSetYEnd(&window_tsensor_edit, 238);

//     const uint8_t btn_w = 90;
//     const uint8_t btn_h = 50;
//     const uint16_t width = UG_WindowGetInnerWidth(&window_tsensor_edit);
//     const uint16_t height = UG_WindowGetInnerHeight(&window_tsensor_edit);
//     const uint16_t xb = 5;
//     uint8_t y = 3;
//     UG_ButtonCreate(&window_tsensor_edit, &button_tsensor_edit_back, BTN_ID_BACK, xb, y, xb + btn_w, y + btn_h);
//     UG_ButtonSetFont(&window_tsensor_edit, BTN_ID_BACK, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_tsensor_edit, BTN_ID_BACK, (char*)"Назад");
//     y += btn_h + 2;
//     UG_ButtonCreate(&window_tsensor_edit, &button_tsensor_edit_delete, BTN_ID_0, xb, y, xb + btn_w, y + btn_h);
//     UG_ButtonSetFont(&window_tsensor_edit, BTN_ID_0, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_tsensor_edit, BTN_ID_0, (char*)"Удалить");
//     y += btn_h + 2;
//     UG_ButtonCreate(&window_tsensor_edit, &button_tsensor_edit_onoff, BTN_ID_1, xb, y, xb + btn_w, y + btn_h);
//     UG_ButtonSetFont(&window_tsensor_edit, BTN_ID_1, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_tsensor_edit, BTN_ID_1, (char*)"Вкл/Выкл");
//     y += btn_h + 2;
//     UG_ButtonCreate(&window_tsensor_edit, &button_tsensor_edit_find, BTN_ID_2, xb, y, xb + btn_w, y + btn_h);
//     UG_ButtonSetFont(&window_tsensor_edit, BTN_ID_2, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_tsensor_edit, BTN_ID_2, (char*)"Найти");
//     y = 10;
//     UG_Custom_Object_Create(&window_tsensor_edit, &object_tsensor_addr, OBJ_ID_0, btn_w + 15, y, width - 35, height - 50, saltan_gui_tsensor_addr_draw);
// }

// static void create_param_window(void)
// {
//     UG_WindowCreate(&window_param, obj_buff_wnd_param_calib, PARAM_MAX_OBJECTS, window_param_callback);
//     UG_WindowSetTitleText(&window_param, (char*)"Настройка параметров");
//     UG_WindowSetTitleTextFont(&window_param, &FONT_8X14_RU);

//     UG_WindowSetXEnd(&window_param, 320);
//     UG_WindowSetYEnd(&window_param, 238);

//     const uint8_t btn_width = 90;
//     const uint8_t btn_height = 50;
//     const uint16_t width = UG_WindowGetInnerWidth(&window_tsensor_edit);
//     const uint16_t height = UG_WindowGetInnerHeight(&window_tsensor_edit);
//     uint16_t x = 5;
//     uint8_t y = 3;

//     UG_Custom_Object_Create(&window_param, &object_param, OBJ_ID_0, btn_width + 15, 30, width - btn_width - 5, height - 5, saltan_gui_param_draw);

//     UG_ButtonCreate(&window_param, &button_param_back, BTN_ID_BACK, x, y, x + btn_width, y + btn_height);
//     UG_ButtonSetFont(&window_param, BTN_ID_BACK, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_param, BTN_ID_BACK, (char*)"Назад");
//     y += btn_height + 2;
//     UG_ButtonCreate(&window_param, &button_zero, BTN_ID_0, x, y, x + btn_width, y + btn_height);
//     UG_ButtonSetFont(&window_param, BTN_ID_0, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_param, BTN_ID_0, (char*)"Вверх");
//     y += btn_height + 2;
//     UG_ButtonCreate(&window_param, &button_dac_up, BTN_ID_1, x, y, x + btn_width, y + btn_height);
//     UG_ButtonSetFont(&window_param, BTN_ID_1, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_param, BTN_ID_1, (char*)"Вниз");
//     y += btn_height + 2;
//     UG_ButtonCreate(&window_param, &button_dac_down, BTN_ID_2, x, y, x + btn_width, y + btn_height);
//     UG_ButtonSetFont(&window_param, BTN_ID_2, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_param, BTN_ID_2, (char*)"Править");
// }

// static void create_errors_window(void)
// {
//     UG_WindowCreate(&window_errors, obj_buff_wnd_errors, ERRORS_MAX_OBJECTS, window_errors_callback);
//     UG_WindowSetTitleText(&window_errors, (char*)"Ошибки");
//     UG_WindowSetTitleTextFont(&window_errors, &FONT_8X14_RU);

//     UG_WindowSetXEnd(&window_errors, 320);
//     UG_WindowSetYEnd(&window_errors, 238);

//     const uint8_t btn_w = 70;
//     const uint8_t btn_h = 50;
//     const uint16_t width = UG_WindowGetInnerWidth(&window_tsensor_edit);
//     const uint16_t height = UG_WindowGetInnerHeight(&window_tsensor_edit);
//     const uint16_t xb = 5;
//     uint8_t y = 3;
//     UG_ButtonCreate(&window_errors, &button_errors_back, BTN_ID_BACK, xb, y, xb + btn_w, y + btn_h);
//     UG_ButtonSetFont(&window_errors, BTN_ID_BACK, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_errors, BTN_ID_BACK, (char*)"Назад");
//     y += btn_h + 2;
//     UG_ButtonCreate(&window_errors, &button_pgdown, BTN_ID_0, xb, y, xb + btn_w, y + btn_h);
//     UG_ButtonSetFont(&window_errors, BTN_ID_0, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_errors, BTN_ID_0, (char*)"Вверх");
//     y += btn_h + 2;
//     UG_ButtonCreate(&window_errors, &button_pgup, BTN_ID_1, xb, y, xb + btn_w, y + btn_h);
//     UG_ButtonSetFont(&window_errors, BTN_ID_1, &FONT_8X14_RU);
//     UG_ButtonSetText(&window_errors, BTN_ID_1, (char*)"Вниз");
//     y = 20;
//     UG_Custom_Object_Create(&window_errors, &object_errors, OBJ_ID_0, btn_w + 15, y, width - btn_w - 5, height - 5, saltan_gui_errors_draw);
// }

static uint8_t saltan_gui_fill_frame(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t color)
{
    uint16_t w = (x2 - x1 > 0) ? (x2 - x1) : (x1 - x2);
    uint16_t h = (y2 - y1 > 0) ? (y2 - y1) : (y1 - y2);
    if(w == 0) w = 1;
    if(h ==0) h = 1;
    ILI9341_FillRectangle(x1, y1, w, h, color);
    return 0;
}

static uint8_t saltan_gui_draw_line(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t color)
{
    uint16_t w = (x2 - x1 > 0) ? (x2 - x1) : (x1 - x2);
    uint16_t h = (y2 - y1 > 0) ? (y2 - y1) : (y1 - y2);
    if(w == 0) w = 1;
    if(h ==0) h = 1;
    ILI9341_FillRectangle(x1, y1, w, h, color);
    return 0;
}

// static void push_button_back(void)
// {
//     UG_PushButton(BTN_ID_BACK);
// }

// static void push_button_0(void)
// {
//     UG_PushButton(BTN_ID_0);
// }

// static void push_button_1(void)
// {
//     UG_PushButton(BTN_ID_1);
// }

// static void push_button_2(void)
// {
//     UG_PushButton(BTN_ID_2);
// }

// static void push_button_3(void)
// {
//     UG_PushButton(BTN_ID_3);
// }

// void show_setup_window(void)
// {
//     UG_WindowShow(&window_setup);
//     kbd_set_cb_1(&push_button_back);
//     kbd_set_cb_2(&push_button_0); // temp
//     kbd_set_cb_3(&push_button_1); // param
//     kbd_set_cb_4(&push_button_2); // errors
// }

// soft_timer_t* voltage_timer = NULL;
// void show_voltage_window(void)
// {
//     voltage_gui_update_view();
//     UG_WindowShow(&window_voltage);
//     kbd_set_cb_1(&push_button_back);
//     kbd_set_cb_2(&push_button_0);
//     kbd_set_cb_3(&push_button_1);
//     kbd_set_cb_4(&push_button_2);
//     if(!voltage_timer)
//         voltage_timer = soft_timer(3, voltage_gui_update_view, false, false);
//     else
//         soft_timer_restart(voltage_timer);
// }

// void show_temp_window(void)
// {
//     UG_WindowShow(&window_temp);
//     kbd_set_cb_1(&push_button_back);
//     kbd_set_cb_2(NULL);
//     kbd_set_cb_3(NULL);
//     kbd_set_cb_4(NULL);
// }

// void back_to_status_window(void);
// soft_timer_t* iotimer = NULL;
// void show_iostate_window(void)
// {
//     io_gui_update_view();
//     pcf_enter_test();
//     UG_WindowShow(&window_iostate);
//     kbd_set_cb_1(&push_button_back);
//     kbd_set_cb_2(&push_button_0);
//     kbd_set_cb_3(&push_button_1);
//     kbd_set_cb_4(&push_button_2);
//     if(!iotimer)
//         iotimer = soft_timer(5, back_to_status_window, true, false);
//     else
//         soft_timer_restart(iotimer);
// }

// void show_tsensor_window(void)
// {
//     tsensor_gui_update_view();
//     UG_WindowShow(&window_tsensor);
//     kbd_set_cb_1(&push_button_back);
//     kbd_set_cb_2(&push_button_0);      // tsensors_up
//     kbd_set_cb_3(&push_button_1);    // tsensors_down
//     kbd_set_cb_4(&push_button_2);   //
// }

// static void set_approve_cb(void (*cb)(void), void (*go_back)(void))
// {
//     approve_cb = cb;
//     back_cb = go_back;
// }

// static void show_approve_window(void)
// {
//     UG_WindowShow(&window_approve);
//     kbd_set_cb_1(&push_button_back); // no
//     kbd_set_cb_2(NULL);
//     kbd_set_cb_3(NULL);
//     kbd_set_cb_4(&push_button_1); // yes
// }

// static void show_tsensor_edit_window(void)
// {
//     UG_WindowShow(&window_tsensor_edit);
//     kbd_set_cb_1(&push_button_back);
//     kbd_set_cb_2(&push_button_0); // delete addr
//     kbd_set_cb_3(&push_button_1); // tsensor onoff
//     kbd_set_cb_4(&push_button_2); // tsensor find
// }

// static void show_param_window(void)
// {
//     opt_list_t* opt_list = ai_opt_list();
//     options_update_view(opt_list);
//     UG_WindowShow(&window_param);
//     kbd_set_cb_1(&push_button_back);
//     kbd_set_cb_2(&push_button_0); // set zero
//     kbd_set_cb_3(&push_button_1); // set max
//     kbd_set_cb_4(&push_button_2);
// }

// static void show_errors_window(void)
// {
//     UG_WindowShow(&window_errors);
//     kbd_set_cb_1(&push_button_back);
//     kbd_set_cb_2(&push_button_0);
//     kbd_set_cb_3(&push_button_1);
//     kbd_set_cb_4(NULL);
// }

static void show_status_window(void)
{
    UG_WindowShow(&window_status);
    // kbd_set_cb_1(&push_button_0);
    // kbd_set_cb_2(&push_button_1);
    // kbd_set_cb_3(&push_button_2);
    // kbd_set_cb_4(&push_button_3);
}

// void create_img_window(void)
// {
//    UG_WindowCreate( &window_2, obj_buff_wnd_2, MAX_OBJECTS, window_2_callback );
//    UG_WindowSetTitleText( &window_2, "Info" );
//    UG_WindowSetTitleTextFont( &window_2, &FONT_12X20 );
//    UG_WindowResize( &window_2, 20, 40, 219, 279 );

//    UG_ImageCreate( &window_2, &image2_1, IMG_ID_0, (UG_WindowGetInnerWidth( &window_2 )>>1) - (logo.width>>1), 40, 0, 0 );
//    UG_ImageSetBMP( &window_2, IMG_ID_0, &logo );
// }

void saltan_gui_init(void)
{
    UG_Init(&gui, (void(*)(UG_S16, UG_S16, UG_COLOR))ILI9341_DrawPixel, 320, 240);
    /* Register hardware acceleration */
    UG_DriverRegister(DRIVER_DRAW_LINE, saltan_gui_draw_line);
    UG_DriverEnable(DRIVER_DRAW_LINE);
    UG_DriverRegister(DRIVER_FILL_FRAME, saltan_gui_fill_frame);
    UG_DriverEnable(DRIVER_FILL_FRAME);

    create_status_window();
    // create_approve_window();
    // create_iostate_window();
    // create_temperature_window();
    // create_voltage_window();
    // create_setup_window();
    // create_tsensor_window();
    // create_tsensor_edit_window();
    // create_param_window();
    // create_errors_window();

    show_status_window();
}

// static void window_approve_callback(UG_MESSAGE* msg)
// {
//     if (msg->type == MSG_TYPE_OBJECT)
//     {
//         if ((msg->id    == OBJ_TYPE_BUTTON)
//          && (msg->event == OBJ_EVENT_RELEASED))
//         {
//             switch(msg->sub_id)
//             {
//                 case BTN_ID_BACK:
//                 {
//                     back_cb();
//                     break;
//                 }
//                 case BTN_ID_1:
//                 {
//                     approve_cb();
//                     back_cb();
//                     break;
//                 }
//                 default:
//                     break;
//             }
//         }
//     }
// }

static void window_status_callback(UG_MESSAGE* msg)
{
    if (msg->type == MSG_TYPE_OBJECT)
    {
        if ((msg->id    == OBJ_TYPE_BUTTON)
         && (msg->event == OBJ_EVENT_RELEASED))
        {
            switch(msg->sub_id)
            {
                case BTN_ID_0:
                {
                    // show_iostate_window();
                    break;
                }
                case BTN_ID_1:
                {
                    // show_voltage_window();
                    break;
                }
                case BTN_ID_2:
                {
                    // show_temp_window();
                    break;
                }
                case BTN_ID_3:
                {
                    // show_setup_window();
                    break;
                }
                default:
                    break;
            }
        }
    }
}

// void back_to_status_window(void)
// {
//     if(soft_timer_is_active(iotimer))
//         soft_timer_stop(iotimer);
//     pcf_exit_test();
//     is_io_test = false;
//     show_status_window();
// }

// static void window_iostate_callback(UG_MESSAGE* msg)
// {
//     if(msg->type == MSG_TYPE_OBJECT)
//     {
//         if ((msg->id    == OBJ_TYPE_BUTTON)
//          && (msg->event == OBJ_EVENT_RELEASED))
//         {
//             switch(msg->sub_id)
//             {
//                 case BTN_ID_BACK:
//                 {
//                     back_to_status_window();
//                     break;
//                 }
//                 case BTN_ID_0:
//                 {
//                     if(iotimer)
//                         soft_timer_restart(iotimer);
//                     io_gui_prev();
//                     object_iostate->state |= OBJ_STATE_UPDATE;
//                     break;
//                 }
//                 case BTN_ID_1:
//                 {
//                     if(iotimer)
//                         soft_timer_restart(iotimer);
//                     io_gui_next();
//                     object_iostate->state |= OBJ_STATE_UPDATE;
//                     break;
//                 }
//                 case BTN_ID_2:
//                 {
//                     if(iotimer)
//                         soft_timer_restart(iotimer);
//                     io_gui_switch_state();
//                     object_iostate->state |= OBJ_STATE_UPDATE;
//                     break;
//                 }
//                 default:
//                     break;
//             }
//         }
//     }
// }

// static void window_voltage_callback(UG_MESSAGE* msg)
// {
//     if (msg->type == MSG_TYPE_OBJECT)
//     {
//         if ((msg->id    == OBJ_TYPE_BUTTON)
//          && (msg->event == OBJ_EVENT_RELEASED))
//         {
//             switch(msg->sub_id)
//             {
//                 case BTN_ID_BACK:
//                 {
//                     if(soft_timer_is_active(voltage_timer))
//                         soft_timer_stop(voltage_timer);
//                     show_status_window();
//                     break;
//                 }
//                 case BTN_ID_0:
//                 {
//                     voltage_gui_prev();
//                     object_voltage->state |= OBJ_STATE_UPDATE;
//                     break;
//                 }
//                 case BTN_ID_1:
//                 {
//                     voltage_gui_next();
//                     object_voltage->state |= OBJ_STATE_UPDATE;
//                     break;
//                 }
//                 case BTN_ID_2:
//                 {
//                     voltage_gui_switch_state();
//                     object_voltage->state |= OBJ_STATE_UPDATE;
//                     break;
//                 }
//                 default:
//                     break;
//             }
//         }
//     }
// }

// static void window_temp_callback(UG_MESSAGE* msg)
// {
//     if (msg->type == MSG_TYPE_OBJECT)
//     {
//         if ((msg->id    == OBJ_TYPE_BUTTON)
//          && (msg->event == OBJ_EVENT_RELEASED))
//         {
//             switch(msg->sub_id)
//             {
//                 case BTN_ID_BACK:
//                 {
//                     show_status_window();
//                     break;
//                 }
//                 default:
//                     break;
//             }
//         }
//     }
// }

// void window_setup_callback(UG_MESSAGE* msg)
// {
//     if (msg->type == MSG_TYPE_OBJECT)
//     {
//         if ((msg->id    == OBJ_TYPE_BUTTON)
//          && (msg->event == OBJ_EVENT_RELEASED))
//         {
//             switch(msg->sub_id)
//             {
//                 case BTN_ID_BACK:
//                 {
//                     show_status_window();
//                     break;
//                 }
//                 case BTN_ID_0:
//                 {
//                     show_tsensor_window();
//                     break;
//                 }
//                 case BTN_ID_1:
//                 {
//                     show_param_window();
//                     break;
//                 }
//                 case BTN_ID_2:
//                 {
//                     show_errors_window();
//                     break;
//                 }
//                 default:
//                     break;
//             }
//         }
//     }
// }

// void window_tsensor_callback(UG_MESSAGE* msg)
// {
//    if (msg->type == MSG_TYPE_OBJECT)
//    {
//         if ((msg->id    == OBJ_TYPE_BUTTON)
//          && (msg->event == OBJ_EVENT_RELEASED))
//         {
//             switch(msg->sub_id)
//             {
//                 case BTN_ID_BACK:
//                 {
//                     rom_save_data();
//                     show_setup_window();
//                     break;
//                 }
//                 case BTN_ID_0:
//                 {
//                     tsensor_gui_up();
//                     object_tsensors_list->state |= OBJ_STATE_UPDATE;
//                     break;
//                 }
//                 case BTN_ID_1:
//                 {
//                     tsensor_gui_down();
//                     object_tsensors_list->state |= OBJ_STATE_UPDATE;
//                     break;
//                 }
//                 case BTN_ID_2:
//                 {
//                     show_tsensor_edit_window();
//                     break;
//                 }
//                 default:
//                     break;
//             }
//         }
//     }
// }

// static void on_tsensor_gui_clear_addr(void)
// {
//     uint8_t id = tsensor_gui_selected_id();
//     tsensor_clear_addr(id);
//     tsensor_gui_update_item();
//     tsensor_gui_update_view();
//     object_tsensor_addr->state |= OBJ_STATE_UPDATE;
//     object_tsensors_list->state |= OBJ_STATE_UPDATE;
// }

// static void on_tsensor_gui_find_addr(void)
// {
//     uint8_t id = tsensor_gui_selected_id();
//     tsensor_find_addr(id);
//     tsensor_gui_update_item();
//     tsensor_gui_update_view();
//     object_tsensor_addr->state |= OBJ_STATE_UPDATE;
//     object_tsensors_list->state |= OBJ_STATE_UPDATE;
// }

// void window_tsensor_edit_callback(UG_MESSAGE* msg)
// {
//    if (msg->type == MSG_TYPE_OBJECT)
//    {
//         if ((msg->id    == OBJ_TYPE_BUTTON)
//          && (msg->event == OBJ_EVENT_RELEASED))
//         {
//             switch(msg->sub_id)
//             {
//                 case BTN_ID_BACK:
//                 {
//                     show_tsensor_window();
//                     break;
//                 }
//                 case BTN_ID_0:
//                 {
//                     set_approve_cb(&on_tsensor_gui_clear_addr, &show_tsensor_edit_window);
//                     show_approve_window();
//                     break;
//                 }
//                 case BTN_ID_1:
//                 {
//                     uint8_t id = tsensor_gui_selected_id();
//                     tsensor_switch(id, !tsensor_is_active(id));
//                     tsensor_gui_update_item();
//                     object_tsensor_addr->state |= OBJ_STATE_UPDATE;
//                     break;
//                 }
//                 case BTN_ID_2:
//                 {
//                     set_approve_cb(&on_tsensor_gui_find_addr, &show_tsensor_edit_window);
//                     show_approve_window();
//                     break;
//                 }
//                 default:
//                     break;
//             }
//         }
//     }
// }

// static void param_rename_buttons(bool is_edit)
// {
//     if(is_edit)
//     {
//         UG_ButtonSetText(&window_param, BTN_ID_0, (char*)"+");
//         UG_ButtonSetText(&window_param, BTN_ID_1, (char*)"-");
//         UG_ButtonSetText(&window_param, BTN_ID_2, (char*)"OK");
//     }else{
//         UG_ButtonSetText(&window_param, BTN_ID_0, (char*)"Вверх");
//         UG_ButtonSetText(&window_param, BTN_ID_1, (char*)"Вниз");
//         UG_ButtonSetText(&window_param, BTN_ID_2, (char*)"Править");
//     }
// }

// void window_param_callback(UG_MESSAGE* msg)
// {
//     if (msg->type == MSG_TYPE_OBJECT)
//     {
//         if ((msg->id    == OBJ_TYPE_BUTTON)
//             && (msg->event == OBJ_EVENT_RELEASED))
//         {
//             switch(msg->sub_id)
//             {
//                 case BTN_ID_BACK:
//                 {
//                     opt_list_t* opt_list = ai_opt_list();
//                     if(options_is_edit(opt_list))
//                     {
//                         options_edit(opt_list, false);
//                         ai_set_calibration(false, -1);
//                         param_rename_buttons(false);
//                         break;
//                     }
//                     rom_save_data();
//                     show_setup_window();
//                     break;
//                 }
//                 case BTN_ID_0:
//                 {
//                     opt_list_t* opt_list = ai_opt_list();
//                     if(options_is_edit(opt_list))
//                     {
//                         options_inc(opt_list);
//                     }else{
//                         options_up(opt_list);
//                     }
//                     options_update_item(opt_list);
//                     options_update_view(opt_list);
//                     object_param->state |= OBJ_STATE_UPDATE;
//                     break;
//                 }
//                 case BTN_ID_1:
//                 {
//                     opt_list_t* opt_list = ai_opt_list();
//                     if(options_is_edit(opt_list))
//                     {
//                         options_dec(opt_list);
//                     }else{
//                         options_down(opt_list);
//                     }
//                     options_update_item(opt_list);
//                     options_update_view(opt_list);
//                     object_param->state |= OBJ_STATE_UPDATE;
//                     break;
//                 }
//                 case BTN_ID_2:
//                 {
//                     opt_list_t* opt_list = ai_opt_list();
//                     bool is_edit = options_is_edit(opt_list);
//                     options_edit(opt_list, !is_edit);
//                     uint8_t opt_id = options_selected_id(opt_list);
//                     ai_set_calibration(!is_edit, opt_id);
//                     param_rename_buttons(!is_edit);
//                     options_update_view(opt_list);
//                     object_param->state |= OBJ_STATE_UPDATE;
//                     break;
//                 }
//                 default:
//                     break;
//             }
//         }
//     }
// }

// void window_errors_callback(UG_MESSAGE* msg)
// {
//     if (msg->type == MSG_TYPE_OBJECT)
//     {
//         if ((msg->id    == OBJ_TYPE_BUTTON)
//          && (msg->event == OBJ_EVENT_RELEASED))
//         {
//             switch(msg->sub_id)
//             {
//                 case BTN_ID_BACK:
//                 {
//                     show_setup_window();
//                     break;
//                 }
//                 case BTN_ID_0:
//                 {
//                     errors_page_up();
//                     UG_TextboxSetText(&window_errors, TXB_ID_0, errors_page());
//                     break;
//                 }
//                 case BTN_ID_1:
//                 {
//                     errors_page_down();
//                     UG_TextboxSetText(&window_errors, TXB_ID_0, errors_page());
//                     break;
//                 }
//                 default:
//                     break;
//             }
//         }
//     }
// }
