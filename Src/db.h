#ifndef _DB_H_
#define _DB_H_ 1
#include "ff.h"

typedef struct
{
	uint32_t count; // количество записей
    FIL 	 file;
}db_t;

int db_push(char* filename); // записать строку
int db_peek(char* filename, int maxlen); // взять строку, не удаляя из списка
int db_pop(void); // удалить верхнюю строку из списка
int db_init(void);

#endif
