#ifndef _SALTAN_GUI_H_
#define _SALTAN_GUI_H_

void saltan_gui_init(void);

void saltan_gui_update_temp(void);
void saltan_gui_update_voltage(void);
void saltan_gui_update(void);

#endif
