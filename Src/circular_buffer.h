#ifndef __CIRCULAR_BUFFER__
#define __CIRCULAR_BUFFER__ 1
#include <stdint.h>

typedef struct
{
    uint8_t* data;
    uint16_t len;
}message_t;

typedef struct
{
    void *buffer;     // data buffer
    void *buffer_end; // end of data buffer
    size_t count;     // number of items in the buffer
    size_t maxlen;    // maximum number of items in the buffer
    void *head;       // pointer to head
    void *tail;       // pointer to tail
}circular_buffer_t;

#define CIRCULAR_BUFFER_DEF(name, _maxlen)                  \
    uint8_t name##_data_space[_maxlen];                     \
    circular_buffer_t name = {                              \
        .buffer = name##_data_space,                        \
        .buffer_end = (char *)name##_data_space + _maxlen,  \
        .maxlen = _maxlen,                                  \
        .count = 0,                                         \
        .head = name##_data_space,                          \
        .tail = name##_data_space,                          \
    };\

// only data
int cbuffer_push(circular_buffer_t *cbuff, uint8_t *item, size_t size);
int cbuffer_pop(circular_buffer_t *cbuff, uint8_t *item, size_t size);

// len + data
int cbuffer_push_message(circular_buffer_t *cbuff, uint8_t* data, uint16_t len);
int cbuffer_pop_message(circular_buffer_t *cbuff, uint8_t* data);

#endif
