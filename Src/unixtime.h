#ifndef __UNIXTIME_H__
#define __UNIXTIME_H__ 1

void updateRTC(uint64_t now);
uint64_t unixtime(void);

#endif
