#ifndef _INPUT_H_
#define _INPUT_H_ 1

#include "uptime.h"
#include "stdbool.h"
#include "list.h"
#include "usart_port.h"

typedef struct __packed{
	uint16_t len;
	uint16_t id;
	uint16_t type;
	uint64_t timestamp;
}input_msg_header_t;

#define INPUT_MSG_HEADER_LEN sizeof(input_msg_header_t)

void input_on_message(usart_port_t* port);
void input_data(char* string);
void input_push_data(usart_port_t* port, uint8_t* data, uint16_t len); // for uin

// int  input_filter_data(input_t* input, uint8_t* data, int len); // проверка на изменение важных значений
// void input_init(input_t* input);

void inputs_configure(void);

void input_welding_coeffs(int32_t* values);

void input_update_timestamp(void);

#endif
