#include "db.h"
#include <string.h>
#include <stdio.h>
#include <string.h>

db_t fl;
#define MAX_FILES_NUM 20

// text data base 

int db_push(char* data) // записать строку
{
    // записать новую строку в конец файла
    FRESULT res = f_open(&fl.file, ".db", FA_READ | FA_WRITE | FA_OPEN_APPEND);
    if(res != FR_OK) 
        return -1;
    int ret = f_printf(&fl.file, "%s\n", data);
    f_close(&fl.file);
    if(ret <= 0)
        return ret;
    return 0;
}

int db_peek(char* data, int maxlen) // взять данные, не удаляя из списка
{
    int ret = 0;
    FILINFO fno;
    FRESULT res = f_stat(".db", &fno);
    if(res != FR_OK)
        return -1;

    // считать первую строку
    res = f_open(&fl.file, ".db", FA_READ);
    if(res != FR_OK)
        return -1;

    TCHAR* buff = f_gets((TCHAR*)data, maxlen, &fl.file);
    if(buff != NULL)
    {    
        int eof = strlen(data); // exclude '\n'
        if(eof > 1)
            data[eof - 1] = 0;
    }else{
        ret = -2;
    }
    f_close(&fl.file);
    return ret;
}

int db_pop(void) // удалить верхнее имя файла из списка
{
    #define MAX 256
    FIL fptr1 = {0}, fptr2 = {0};
    char fname[] = ".db";
    char str[MAX], temp[] = ".temp";

    FRESULT res = f_open(&fptr1, fname, FA_READ);
    if(res != FR_OK)
    {
        return -1;
    }
    res = f_open(&fptr2, temp, FA_READ | FA_WRITE | FA_OPEN_ALWAYS);
    if(res != FR_OK)
    {
        f_close(&fptr1);
        return -2;
    }
    // copy all contents to the temporary file except first line
    f_gets(str, MAX, &fptr1);
    while(!f_eof(&fptr1))
    {
        f_gets(str, MAX, &fptr1);
        f_printf(&fptr2, "%s", str);
    }
    f_close(&fptr1);
    f_close(&fptr2);
    f_unlink(fname);          // remove the original file 
    f_rename(temp, fname);    // rename the temporary file to original name    
    return 0;
}

int db_init(void)
{
    // nothing
    return 0;
}
