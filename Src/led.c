#include <stdint.h>
#include "stm32f4xx_hal.h"
#include "led.h"

void led_on(uint16_t LED)
{
	HAL_GPIO_WritePin(GPIOB, LED, GPIO_PIN_SET);
}

void led_off(uint16_t LED)
{
	HAL_GPIO_WritePin(GPIOB, LED, GPIO_PIN_RESET);
}

void led_switch(uint16_t LED)
{
	HAL_GPIO_TogglePin(GPIOB, LED);
}

void led_init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14|GPIO_PIN_15, GPIO_PIN_RESET);

	/*Configure GPIO pins : PB14 PB15 */
	GPIO_InitStruct.Pin = GPIO_PIN_14|GPIO_PIN_15;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}

#include "sdcard.h"
#include "sadko.h"
#include "log_sd.h"
#include "stdio.h"
void led_task(void)
{
	if(sdcard_status())
	{
		logger_deinit();
		sd_reinit();
		sadko_reset_coeffs();
		led_off(REC_LED);
	}else{
		led_on(REC_LED);
	}

	led_off(OUTPUT_LED);
}
