#ifndef _LIST_H_
#define _LIST_H_ 1
#include "stdint.h"
#include "stddef.h"

typedef struct item_s
{
    void *data;
    struct item_s* next;
}item_t;

typedef struct list_s list_t;

struct list_s
{
    uint32_t size;
    item_t *first;
    item_t *last;
};

list_t* list_init(void);
void list_destroy(list_t *list);

void list_first(list_t *list, item_t** item);
uint8_t list_next(list_t *list, item_t** item);
uint8_t list_next_item(item_t** item);
void list_prev(list_t *list, item_t** item); // do not uuse in long lists // optimized for item size
int list_eol(list_t *list);
void * list_data(list_t *list, item_t** item);
int list_is_data(list_t *list, item_t** item);
size_t list_size(list_t *list);
void list_insert_head(list_t *list, void *data);
void list_insert_tail(list_t *list, void *data);

void list_remove_item(list_t *list, void *data);

#endif /* _LIST_H_ */
