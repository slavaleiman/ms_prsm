
#include "version.h"
#include "sadko.h"
#include "input.h"
#include <stdio.h>
#include <string.h>
#include "packet_types.h"

#ifndef UNITTEST
#include "usart_port.h"
#include "modbus.h"
#include "errors.h"
#include "CRC.h"
#include "circular_buffer.h"
#include "sdcard.h"
#include "ff.h"
#include "log_sd.h"
#include "file_list.h"
#include <stdlib.h>
#include "unixtime.h"
#include "temperature.h"
#include "led.h"

extern usart_port_t uport7; // output

#endif

void update_input_regs(void);

#define SADKO_MAX_MSG_LEN (125 * 2) // почему 125 см. doc/mb_protocol.doc (протокол садко)

// здесь лежат сообщения на выходе 
// буфер должен обновляться после кадого подтверждения отправки

// CIRCULAR_BUFFER_DEF(sadko_cbuffer, SADKO_CBUFFER_SIZE)

sadko_t sadko;

#ifndef UNITTEST

void sadko_send_complete(void)
{
    if(sadko.uport->is_sending)
    {
        sadko.uport->is_sending = false;
        HAL_GPIO_WritePin(SADKO_ENABLE_PORT, SADKO_ENABLE_PIN, 1); // DE=1 driver disable 
    }
}

void sadko_send_response(void)
{
    HAL_GPIO_WritePin(SADKO_ENABLE_PORT, SADKO_ENABLE_PIN, 0);
    
    uport_transmit(sadko.uport);
#ifndef CHECK_TX
    if(sadko.uport->timer)
    {
        soft_timer_restart(sadko.uport->timer);
    }else{
        sadko.uport->timer = soft_timer(50, sadko_send_complete, true, false); // sending
    }
#endif
}

void sadko_process_message(void)
{
    int ret = modbus_on_rtu(sadko.uport, sadko.modbus_data, sadko.modbus_data_len);
    if(!ret)
    {
        led_on(OUTPUT_LED);
        if(sadko.timer)
        {
            soft_timer_restart(sadko.timer);
        }else{
            sadko.timer = soft_timer(SADKO_RESPONSE_TIMEOUT, sadko_send_response, true, false); // saving to sd
        }
    }else{
        errors_on_error(ret);
    }
    sadko.on_message_flag = false;
}

void sadko_on_message(usart_port_t* port)
{
    uint8_t* data = port->rx_buffer;
    uint16_t len = port->rx_size;

    sadko.modbus_data = data;
    sadko.modbus_data_len = len;
    sadko.on_message_flag = true;
}

void sadko_set_modbus_addr(uint8_t addr)
{
    sadko.uport->addr = addr;
}

void sadko_task(void)
{
    if(sadko.on_message_flag)
        sadko_process_message();

    if(sadko.update_rtc)
    {
        sadko_update_rtc();
    }

    if(!log_is_init())
        return;
    if(!sd_is_init())
    {
        if(sd_mount())
        {
            return;
        }
    }
    sadko_update();
}

#endif

void save_output_position(void)
{
#ifndef UNITTEST
    uint8_t data[72];
    sadko_settings_t set;

    // тут если не отправлено - то потеря 255 байт !!! 
    // по требованию заказчика (длина в регистрах - делает разными сообщения считываемое и отсылаемое)

    // вычитаем длину еще не отданных пакетов, чтобы при загрузке начать оттуда
    if(sadko.file_read_ptr >= sadko.output_length)
        set.read_ptr = sadko.file_read_ptr - sadko.output_length;
    else
        set.read_ptr = sadko.file_read_ptr;

    set.packet_sended = sadko.file_packets_sended;
    sprintf((char*)&set.filename[0], "%s", sadko.filename);

    memcpy(data, &set, sizeof(sadko_settings_t));
    sd_write_data(".sadko", (uint8_t*)data, sizeof(sadko_settings_t), 0);
#endif
}

uint16_t get_msg_length(uint8_t* msg)
{
    // uint16_t len = ((msg[0] << 8) | msg[1]) + 2; // длина всего сообщения + поле длины (байт)
    uint16_t len = ((msg[0] << 8) | msg[1]) * 2; // длина всего сообщения + поле длины (регистров)
    return len;
}

int shift_output_buffer(uint32_t msg_count)
{
    uint8_t skip = 0; // read msg position
    if(sadko.output_length == 0)
        return 0;
    uint8_t m = 0;
    for(; m < sadko.messages_in_outbuffer; ++m)
    {
        uint16_t len = get_msg_length(&sadko.output_buffer[skip]);
        if(!len)
        { // drop buffer
            sadko.messages_in_outbuffer = 0;
            break;
        }
        skip += len;
        if(msg_count == m)
        {
            break;
        }
    }
    int16_t movelen = sadko.output_length - skip;
    if(skip > 0)
    {
        if(movelen > 0)
        {
            memmove(sadko.output_buffer, &sadko.output_buffer[skip], movelen);
            sadko.output_length = movelen;
        }else{
            memset(sadko.output_buffer, 0, sizeof(sadko.output_buffer));
            sadko.output_length = 0;
        }
    }else{
        sadko.output_length = 0;
        sadko.messages_in_outbuffer = 0;
        return 0;
    }
    if(sadko.messages_in_outbuffer >= m)
    {
        sadko.messages_in_outbuffer -= m;
        sadko.file_packets_sended += m;
    }else{
        sadko.messages_in_outbuffer = 0;
    }
    return skip; // bytes
}

int sadko_drop_file(void) // дропаем старый файл
{
    int ret = 0;
#ifndef UNITTEST
    // TODO check filename
    f_close(&sadko.file);
    f_unlink(sadko.filename); // DELETE SENDED FILE
    sadko.is_file_opened = false;
    ret = file_list_pop();
    sadko.file_read_ptr = 0;
    sadko.output_length = 0;
    sadko.message_length = 0;
    memset(sadko.filename, 0, sizeof(sadko.filename));
    memset(&sadko.fno, 0, sizeof(FILINFO));

    update_input_regs();

#endif
    return ret;
}

int check_can_drop_file(void)
{
#ifndef UNITTEST
    if(sadko.fno.fsize == 0)
    {
        return 1;
    }
    if(!sadko.is_file_opened) // нечего дропать
    {
        return 0;
    }

    if((sadko.file_read_ptr == sadko.fno.fsize) 
    && (sadko.message_length == 0)
    && (sadko.output_length == 0))
    {
        return 1;
    }
#endif
    return 0;
}

// ooooo oooo   oooo oooooooooo ooooo  oooo ooooooooooo 
//  888   8888o  88   888    888 888    88  88  888  88 
//  888   88 888o88   888oooo88  888    88      888     
//  888   88   8888   888        888    88      888     
// o888o o88o    88  o888o        888oo88      o888o    

void update_input_regs(void)
{
#ifndef UNITTEST
    // наверное это не правильно хранить значение в формате для отправки
    sadko.input_regs1[0] = __REV16(0x6);
    sadko.input_regs1[1] = __REV16(VERSION_MINOR);
    sadko.input_regs1[2] = 0;
    sadko.input_regs1[3] = __REV16(125);
    sadko.input_regs1[4] = __REV16(3);
    sadko.input_regs1[5] = __REV16(2);
    // sadko.input_regs1[6] = __REV16(sadko.file_packets - sadko.file_packets_sended);
    sadko.input_regs1[6] = __REV16(sadko.file_packets - sadko.approved_count);

    uint8_t input_state = 0;

    if(log_is_active())
        input_state |= (1 << 0);
    if(log_is_init())
        input_state |= (1 << 1);
    sadko.input_regs1[7] = __REV16(input_state);
    sadko.input_regs1[8] = __REV16(temperature());

    if(sadko.coeffs[0] && sadko.coeffs[1] && sadko.coeffs[2])
    {
        sadko.input_regs1[9] =  __REV16(sadko.coeffs[0]);
        sadko.input_regs1[10] = __REV16(sadko.coeffs[1]);
        sadko.input_regs1[11] = __REV16(sadko.coeffs[2]);
    }else{
        // defaults
        sadko.input_regs1[9] = __REV16(800); 
        sadko.input_regs1[10] = __REV16(500);
        sadko.input_regs1[11] = __REV16(250);
    }

    input_update_timestamp();
#endif
}

void sadko_reset_coeffs(void)
{
    sadko.coeffs[0] = 0;
    sadko.coeffs[1] = 0;
    sadko.coeffs[2] = 0;
    sadko.is_config_read = false;
}

void sadko_process_approve(void)
{
    int skip = shift_output_buffer(sadko.approved_count);
    if(skip > 0)
    {
        save_output_position();
        update_input_regs();
    }
    sadko.approved_count = 0;
}

void sadko_recieve_approve(uint16_t from_id, uint16_t to_id)
{
    // если пришли не правильные ID - ничего не двигаем
    uint32_t count = to_id - from_id + 1;
    sadko.approved_count = count;
    sadko.output_ready = false;
}

int sadko_write_data(uint16_t* buffer, uint16_t from_addr, uint8_t num_regs)
{
#ifndef UNITTEST
    uint16_t offset = from_addr & 0xFFF;
    if((from_addr & 0xF000) == 0x1000)
    {
        memcpy(&sadko.holding_regs1[offset], buffer, num_regs * 2);
        // sadko.command call update read_index here !!!
        if(num_regs > 1)
        {
            uint16_t cmd = __REVSH(buffer[0]);
            if((cmd == CMD_APPROVE))
            {
                uint16_t first_id = __REVSH(buffer[1]);
                uint16_t last_id = __REVSH(buffer[2]);
                sadko_recieve_approve(first_id, last_id);
            }
        }
        // при подтверждении отправки
    }
    else if((from_addr & 0xF000) == 0x2000)
    {   
        // 0x2000 // uint16_t  Широта – градусы
        // 0x2001 // uint16_t  Широта – минуты
        // 0x2002 // uint16_t  Широта - десятитысячные доли минут
        // 0x2003 // uint16_t  Долгота – градусы
        // 0x2004 // uint16_t  Долгота – минуты
        // 0x2005 // uint16_t  Долгота - десятитысячные доли минут
        // 0x2006 // uint16_t  Код символа “E” для восточной или “W” для западной долготы
        // 0x2007 // uint16_t  Код символа “N” для северной или “S” для южной широты

        // 0x2008 int64_t      Текущее время GPS (формат UnixTime UTC), байты 1-2 (младшие)
        // 0x2009 int64_t      Текущее время GPS (формат UnixTime UTC), байты 3-4
        // 0x200A int64_t      Текущее время GPS (формат UnixTime UTC), байты 5-6
        // 0x200B int64_t      Текущее время GPS (формат UnixTime UTC), байты 7-8
        memcpy(&sadko.holding_regs2[offset], buffer, num_regs * 2);
        if(sadko.holding_regs2[8] 
        || sadko.holding_regs2[9] 
        || sadko.holding_regs2[10] 
        || sadko.holding_regs2[11])
        {
            if(!log_is_active())
                sadko.update_rtc = true;
        }
    }
    else if((from_addr & 0xF000) == 0x8000)
    {
        memcpy(&sadko.holding_regs8[offset], buffer, num_regs * 2);
    }
#endif
    return 0;
}

int sadko_read_input(uint16_t *buffer, uint16_t from_addr, uint8_t num_regs)
{
    // data address
    if((from_addr & 0xF000) == 0x2000)
    {
        if(sadko.output_ready)
            memcpy(buffer, sadko.output_buffer, num_regs * 2);
        else
            memset(buffer, 0, num_regs * 2);
    }
    else if((from_addr & 0xF000) == 0x1000)
    {
        if(num_regs <= sizeof(sadko.input_regs1) / 2)
        {
            uint16_t offset = from_addr & 0xFFF;
            memcpy(buffer, &sadko.input_regs1[offset], num_regs * 2); // /* 2 = sizeof(uint16_t) */
        }else{
            return -1;
        }
    }
    return 0;
}

int file_peek(void)
{
    int ret = 0;
#ifndef UNITTEST
    char data[50];
    ret = file_list_peek(data, 50);
    if(!ret)
    {
        // если считанное имя не совпадает со считанным из конфига - дропаем позицию.
        char* filename = strtok(data, ",");
        if(strcmp(&sadko.filename[strlen(LOG_DIR)+1], filename))
        { // если имя обновилось
            sadko.file_read_ptr = 0;
            sadko.file_packets_sended = 0;
            sprintf(sadko.filename, "%s/%s", LOG_DIR, filename);
        }
        char* pktcnt = strtok(NULL, ",");
        if(pktcnt)
            sadko.file_packets = atoi(pktcnt);
    }else{
        // если файл лист пустой
        sadko.filename[0] = 0;
        sadko.file_read_ptr = 0;
        sadko.file_packets = 0;
        sadko.file_packets_sended = 0;
    }
#endif
    return ret;
}

#ifndef UNITTEST
int sadko_read_config(void)
{
    uint8_t data[sizeof(sadko_settings_t)] = {0};
    int ret = sd_read_data(".sadko", (uint8_t*)&data, sizeof(sadko_settings_t), 0);
    if(!ret)
    {
        sadko_settings_t* set = (sadko_settings_t*)data;
        sadko.file_read_ptr = set->read_ptr;
        sadko.file_packets_sended = set->packet_sended;
        strcpy(sadko.filename, (char*)set->filename);
        // sprintf(sadko.filename, "%s", set->filename);
    }else{
        sadko.file_read_ptr = 0;
        sadko.file_packets_sended = 0;
    }

    input_welding_coeffs(sadko.coeffs);

    return ret;
}
#endif

int check_file_exist_and_non_empty(char* path)
{
#ifndef UNITTEST
    FILINFO *fno = &sadko.fno;
    FRESULT res = f_stat(path, fno);
    if(res == FR_NO_FILE)
    {
        return 1;
    }
    if(res == FR_DISK_ERR)
    {
        return -1; // leads to reinit sd
    }
    if(!fno->fsize)
    {
        return 1; // file empty
    }
    if(fno->fsize == sadko.file_read_ptr)
    {
        return 1;
    }
#endif    
    return 0;
}

#ifndef UNITTEST
int open_file(FIL* file)
{
    do
    {
        int ret = file_peek();
        if(ret < 0)
            return -1;
        // алгоритм исключает появление пустых файлов, но может файл был удален руками людей
        ret = check_file_exist_and_non_empty(sadko.filename);
        if(ret > 0) // no file
        {
            file_list_pop();
            memset(sadko.filename, 0, sizeof(sadko.filename));
            memset(&sadko.fno, 0, sizeof(FILINFO));
        }
        else if(ret < 0)
        {
            return ret; // это что то совсем плохое
        }
        else{
            break;
        }
    }while(1);

    FRESULT res = f_open(file, sadko.filename, FA_READ);
    if(res != FR_OK) 
        return -1;

    return 0;
}
#endif

void repack_kahovka(uint8_t* msg_full, uint16_t *len)
{
    // заголовок уже лежит в сообщении, нужно правильно разложить данные
    uint8_t* msg = &msg_full[INPUT_MSG_HEADER_LEN]; // len + id + msg_type + timestamp
    // заполняем данными после заголовка
    uint8_t result_len = 12;
    uint8_t result[12];

    uint16_t data_length = *len - INPUT_MSG_HEADER_LEN; // длина данных
    // для каховской сварки формат определяется длиной сообщения
    // но если записался мусор (длина не соответствует) - нужно дропнуть сообщение
    if(data_length == 7)
    {
        result[0] = 0;
        result[1] = msg[2];
        
        result[2] = msg[3];
        result[3] = msg[4];

        result[4] = 0;
        result[5] = msg[5];
        
        result[6] = 0;
        result[7] = msg[6];
        
        result[8] = 0;
        result[9] = msg[7];

        result[10] = 0;
        result[11] = msg[8];
    }
    else if(data_length == 11)
    {
        result[0] = 0;
        result[1] = msg[2];
        
        result[2] = msg[3];
        result[3] = msg[4];

        result[4] = msg[5];
        result[5] = msg[6];
        
        result[6] = msg[7];
        result[7] = msg[8];
        
        result[8] = msg[9];
        result[9] = msg[10];
        
        result[10] = msg[11];
        result[11] = msg[12];
    }else{
        *len = *len + *len % 2;
        return;
    }
    // тут должно быть чётное количество байт
    // так как в заголовке длина указана в количестве регистров
    memcpy(&msg_full[INPUT_MSG_HEADER_LEN], result, result_len);
    *len = result_len + INPUT_MSG_HEADER_LEN;
}

void repack_uin(uint8_t* msg_full, uint16_t *len)
{
    // заголовок уже лежит в сообщении, нужно правильно разложить данные
    uint8_t* msg = &msg_full[INPUT_MSG_HEADER_LEN]; // len + id + msg_type + timestamp
    // заполняем данными после заголовка
    const uint8_t result_len = 34;
    uint8_t result[result_len];
    uint16_t data_length = *len - INPUT_MSG_HEADER_LEN; // длина данных
    if(data_length > 0)
    {
        // в пакете данные лежат последовательно
        result[0] = msg[0]; // 0
        result[1] = msg[1];
        result[2] = msg[2]; // 1
        result[3] = msg[3];
        result[4] = msg[4]; // 2
        result[5] = msg[5]; // -----------------
        result[6] = msg[8]; // 4
        result[7] = msg[9];
        result[8] = msg[10]; // 5
        result[9] = msg[11];
        result[10] = msg[12]; // 6
        result[11] = msg[13];
        result[12] = msg[14]; // 7
        result[13] = msg[15];
        result[14] = msg[16]; // 8
        result[15] = msg[17]; // -----------------
        result[16] = msg[20]; // A 
        result[17] = msg[21];
        result[18] = msg[22]; // B
        result[19] = msg[23];
        result[20] = msg[24]; // C
        result[21] = msg[25]; // --------------- 
        result[22] = msg[32]; // 0x10
        result[23] = msg[33]; // 0x10
    }else{
        *len = *len + *len % 2;
        return;
    }
    memcpy(&msg_full[INPUT_MSG_HEADER_LEN], result, result_len);
    *len = result_len + INPUT_MSG_HEADER_LEN;
}

// тут нужно перепаковать сообщение в регистры в зависимости от формата
void sadko_process_output_format(uint8_t* msg_full, uint16_t *len)
{
    // по требованию заказчика - длина сообщения должна храниться в ячейках по 2байта (1регистр=2байт)
    uint16_t reg_length = (*len + 1) / 2;
    msg_full[0] = reg_length >> 8;
    msg_full[1] = reg_length & 0xFF;

    uint16_t msg_type = (msg_full[4] << 8) | msg_full[5];

    switch(msg_type)
    {
        case KAHOVKA_PACKET:
        {
            repack_kahovka(msg_full, len); // len в байтах
            break;
        }
        case UIN_PACKET:
        {
            repack_uin(msg_full, len); // len в байтах
            break;
        }
    }
}

int sadko_file_prepare(void)
{
    int ret = 0;
#ifndef UNITTEST        
    if(sadko.approved_count > 0)
    {
        sadko_process_approve();
    }
    if(!sadko.is_file_opened)
    {
        ret = open_file(&sadko.file);
        if(!ret)
            sadko.is_file_opened = true;
        else{
            return -1;
        }
    }
    // если дочитали до конца файла - ждем подтверждения отправки
    if(check_can_drop_file())
    {
        if(!sadko.output_length) // если не осталось данных в буфере
        {
            sadko_drop_file();
        }
        return -2;
    }
    if(sadko.message_length > 255)
        sadko.message_length = 0;
    if(!sadko.message_length)
        ret = sd_read_message(&sadko.file, sadko.file_read_ptr, sadko.message, &sadko.message_length);
    if(ret == -4)
    {   // прочитано 0 байт конец файла
        return ret;
    }
    if(ret < 0) // bad file - switch to next file
    {
        sadko_drop_file(); // если файл был некорректно записан - он дропается
        return ret;
    }
#endif
    return ret; // file ready to read next data
}

// id который не был отправлен
// если он был считан, но не переложен в output_buffer, 
// то считывать его не нужно
// тоесть перед чтением проверять id считанного сообщения !!!
// если сообщение уже было считано в прошлый раз, 
// то пропускаем этап считывания, пробуем его положить в output_buffer

int fill_output_buffer(void)
{
#ifndef UNITTEST
    int16_t free_bytes = 0;
    do{
        if(sadko_file_prepare() < 0)
            break;
        sadko_process_output_format(sadko.message, &sadko.message_length);
        free_bytes = SADKO_MAX_MSG_LEN - (sadko.output_length + sadko.message_length);

        if(free_bytes >= 0)
        {
            // пока есть место в output_buffer, кладем туда сообщения читая их из карты
            memcpy(&sadko.output_buffer[sadko.output_length], sadko.message, sadko.message_length);
            ++sadko.messages_in_outbuffer;
            sadko.file_read_ptr = f_tell(&sadko.file);
            sadko.output_length += sadko.message_length; // + поле длины записи
            update_input_regs();
            // сообщение успешно переложено в outbuffer 
            // можно загружать следующее 
            sadko.message_length = 0;
        }else{
            break;
        }
    }while(free_bytes > 0);

    if(sadko.output_length > 0)
    {
        sadko.output_ready = true; // флаг
    }
#endif    
    return 0;
}

void sadko_update_rtc(void)
{
#ifndef UNITTEST
    // 0x2008 int64_t      Текущее время GPS (формат UnixTime UTC), байты 1-2 (младшие)
    // 0x2009 int64_t      Текущее время GPS (формат UnixTime UTC), байты 3-4
    // 0x200A int64_t      Текущее время GPS (формат UnixTime UTC), байты 5-6
    // 0x200B int64_t      Текущее время GPS (формат UnixTime UTC), байты 7-8

    volatile uint64_t now = __REV16(sadko.holding_regs2[8]) |
                           (__REV16(sadko.holding_regs2[9]) << 16) |
                 ((uint64_t)__REV16(sadko.holding_regs2[0xA]) << 32) |
                 ((uint64_t)__REV16(sadko.holding_regs2[0xB]) << 48);
    updateRTC(now);
    sadko.update_rtc = false;
#endif
}

int sadko_update(void)
{
    int ret = 0;
#ifndef UNITTEST
    if(sadko.output_ready) // буфер заполнен можно отдавать на запрос input_reg output_buffer
    {
        return 0;
    }
    do{
        if(!sadko.is_config_read)
        {
            // читаем имя файла и позицию которые могли устареть
            // если их нет с file_list - они устарели и надо обнулять
            ret = sadko_read_config(); 
            if(ret)
            {
                break;
            }
            sadko.is_config_read = true;
        }
        ret = fill_output_buffer();
        if(ret)
        {
            break;
        }
    }while(0);
    
#endif
    return ret;
}

#ifndef UNITTEST
void sadko_data(char* string)
{
    sprintf(string, "file: %s\n"
                    "size:          %8ld\n"
                    "readptr:       %8ld\n"
                    "outbuffer:     %8d\n"
                    "output_length: %8d\n"
                    "is_file_opened: %d"
                    "packets:        %ld\n"
                    "packets_sended: %ld\n"
                    ,
                     sadko.filename,
                     (uint32_t)sadko.fno.fsize,
                     (uint32_t)sadko.file_read_ptr,
                     sadko.messages_in_outbuffer,
                     sadko.output_length,
                     sadko.is_file_opened,
                     sadko.file_packets,
                     sadko.file_packets_sended
                     );
}

void sadko_update_status(void)
{
    update_input_regs();

    printf( "file: %s\n"
            "size:              %8ld\n"
            "read_ptr:          %8ld\n"
            "packets in buffer: %8d\n"
            "output_length:     %8d\n"
            "is_file_opened:    %d\n"
            "time:              %lx\n"
            "temperature:       %u\n"
            "packets:           %lu\n"
            "packets_sended:    %lu\n"
            "\n",
             sadko.filename,
             (uint32_t)sadko.fno.fsize,
             (uint32_t)sadko.file_read_ptr,
             sadko.messages_in_outbuffer,
             sadko.output_length,
             sadko.is_file_opened,
             RTC->TR,
             temperature(),
             sadko.file_packets,
             sadko.file_packets_sended
             );
}

int sadko_init(usart_port_t* port, uint8_t addr)
{
    // port read write callbacks
    sadko.uport = port;
    sadko.uport->addr = addr;
    sadko.uport->read_data = sadko_read_input;
    sadko.uport->write_data = sadko_write_data;

    sadko.output_ready = false;  // если true -> можно отправлять сообщение в садок
    sadko.is_file_opened = false;
    sadko.is_config_read = false;

    sadko.approved_count = 0;
    
    sadko.timer = NULL;

    memset(sadko.output_buffer, 0, sizeof(sadko.output_buffer));
    return 0;
}
#endif
