#include "main.h"
#include "stm32f4xx_it.h"
#include "usart_port.h"
#include "main.h"
#include "sadko.h"

extern DMA_HandleTypeDef hdma_sdio;
extern DMA_HandleTypeDef hdma_usart1_tx;
extern DMA_HandleTypeDef hdma_usart2_rx;
extern DMA_HandleTypeDef hdma_usart2_tx;
extern DMA_HandleTypeDef hdma_usart3_rx;
extern DMA_HandleTypeDef hdma_usart3_tx;
extern DMA_HandleTypeDef hdma_uart4_rx;
extern DMA_HandleTypeDef hdma_uart4_tx;
extern DMA_HandleTypeDef hdma_usart6_rx;
extern DMA_HandleTypeDef hdma_usart6_tx;


extern PCD_HandleTypeDef hpcd_USB_OTG_FS;
extern DMA_HandleTypeDef hdma_sdio;
extern TIM_HandleTypeDef htim6;
extern SD_HandleTypeDef hsd;

extern usart_port_t uport1;
extern usart_port_t uport2;
extern usart_port_t uport3;

extern DMA_HandleTypeDef hdma_usart1_rx;

void disable_interrupts(void)
{
    // TIM2->CCR3 = PWM_AUDIO_ZERO_LEVEL;
    // TIM2->CCR4 = PWM_AUDIO_ZERO_LEVEL;
    // NVIC_DisableIRQ(TIM2_IRQn);
    // NVIC_DisableIRQ(TIM3_IRQn);
    // NVIC_DisableIRQ(TIM5_IRQn);
    // NVIC_DisableIRQ(TIM7_IRQn);
    // NVIC_DisableIRQ(USART1_IRQn);
    // NVIC_DisableIRQ(USART3_IRQn);
    // NVIC_DisableIRQ(TIM8_UP_TIM13_IRQn);
    // NVIC_DisableIRQ(DMA2_Stream4_IRQn);
  NVIC_DisableIRQ(OTG_FS_IRQn);  
}

void enable_interrupts(void)
{
    // NVIC_EnableIRQ(TIM2_IRQn);
    // NVIC_EnableIRQ(TIM3_IRQn);
    // NVIC_EnableIRQ(TIM5_IRQn);
    // NVIC_EnableIRQ(TIM7_IRQn);
    // NVIC_EnableIRQ(USART1_IRQn);
    // NVIC_EnableIRQ(USART3_IRQn);
    // NVIC_EnableIRQ(TIM8_UP_TIM13_IRQn);
    // NVIC_EnableIRQ(DMA2_Stream4_IRQn);
  NVIC_EnableIRQ(OTG_FS_IRQn);  

}

void NMI_Handler(void)
{
}

void HardFault_Handler(void)
{
  while (1)
  {
  }
}

void MemManage_Handler(void)
{
  while (1)
  {
  }
}

void BusFault_Handler(void)
{
  while (1)
  {
  }
}

void UsageFault_Handler(void)
{
  while (1)
  {
  }
}

void SVC_Handler(void)
{
}

void DebugMon_Handler(void)
{
}

void PendSV_Handler(void)
{
}

void TIM6_DAC_IRQHandler(void)
{
  HAL_TIM_IRQHandler(&htim6);
}

void DMA2_Stream3_IRQHandler(void)
{
  HAL_DMA_IRQHandler(&hdma_sdio);
}

void SDIO_IRQHandler(void)
{
  HAL_SD_IRQHandler(&hsd);
}

void OTG_FS_IRQHandler(void)
{
  HAL_PCD_IRQHandler(&hpcd_USB_OTG_FS);
}

void DMA2_Stream7_IRQHandler(void)
{
  HAL_DMA_IRQHandler(&hdma_usart1_tx);
  usart_on_dma_interrupt(DMA2_Stream7);
}

void UART7_IRQHandler(void)
{
  // HAL_UART_IRQHandler(&huart7);
  // usart_irq_handler(&uport3);
}

/**
  * @brief This function handles UART8 global interrupt.
  */

void USART2_IRQHandler(void)
{
  usart_irq_handler(&uport1);
}

void UART4_IRQHandler(void)
{
  usart_irq_handler(&uport2);
}

void USART3_IRQHandler(void)
{
  // if(!uport3.is_sending)
  // {
    usart_irq_handler(&uport3);
  // }else{
  //   USART3->SR &= ~USART_SR_TC;
  //   sadko_send_complete();
  //   USART3->CR1 &= ~USART_CR1_TCIE;
  //   uport3.dma_input_stream->CR |= DMA_SxCR_TCIE | DMA_SxCR_TEIE | DMA_SxCR_DMEIE;
  // }
}

/**
  * @brief This function handles DMA2 stream2 global interrupt.
//   */
// void DMA2_Stream2_IRQHandler(void)
// {
//   /* USER CODE BEGIN DMA2_Stream2_IRQn 0 */

//   /* USER CODE END DMA2_Stream2_IRQn 0 */
//   HAL_DMA_IRQHandler(&hdma_usart1_rx);
//   /* USER CODE BEGIN DMA2_Stream2_IRQn 1 */
//   usart_on_dma_interrupt(DMA2_Stream2);
//   /* USER CODE END DMA2_Stream2_IRQn 1 */
// }

// // void DMA1_Stream0_IRQHandler(void)
// // {
// //   /* USER CODE BEGIN DMA1_Stream0_IRQn 0 */

// //   /* USER CODE END DMA1_Stream0_IRQn 0 */
// //   HAL_DMA_IRQHandler(&hdma_uart8_tx);
// //   /* USER CODE BEGIN DMA1_Stream0_IRQn 1 */
// //   usart_on_dma_interrupt(DMA1_Stream0);

// //   /* USER CODE END DMA1_Stream0_IRQn 1 */
// // }

// /**
//   * @brief This function handles DMA1 stream1 global interrupt.
//   */
// void DMA1_Stream1_IRQHandler(void)
// {
//   /* USER CODE BEGIN DMA1_Stream1_IRQn 0 */

//   /* USER CODE END DMA1_Stream1_IRQn 0 */
//   HAL_DMA_IRQHandler(&hdma_uart3_rx);
//   /* USER CODE BEGIN DMA1_Stream1_IRQn 1 */
//   usart_on_dma_interrupt(DMA1_Stream1);

//   /* USER CODE END DMA1_Stream1_IRQn 1 */
// }

// *
//   * @brief This function handles DMA1 stream3 global interrupt.
  
void DMA1_Stream3_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Stream3_IRQn 0 */

  /* USER CODE END DMA1_Stream3_IRQn 0 */
  // HAL_DMA_IRQHandler(&hdma_uart3_tx);
  /* USER CODE BEGIN DMA1_Stream3_IRQn 1 */
  usart_on_dma_interrupt(DMA1_Stream3);

  /* USER CODE END DMA1_Stream3_IRQn 1 */
}

// /**
//   * @brief This function handles DMA1 stream6 global interrupt.
//   */
// // void DMA1_Stream6_IRQHandler(void)
// // {
// //   /* USER CODE BEGIN DMA1_Stream6_IRQn 0 */

// //   /* USER CODE END DMA1_Stream6_IRQn 0 */
// //   HAL_DMA_IRQHandler(&hdma_uart8_rx);
// //   /* USER CODE BEGIN DMA1_Stream6_IRQn 1 */

// //   /* USER CODE END DMA1_Stream6_IRQn 1 */
// // }

// // void DMA1_Stream6_IRQHandler(void)
// // {
// //   /* USER CODE BEGIN DMA1_Stream6_IRQn 0 */

// //   /* USER CODE END DMA1_Stream6_IRQn 0 */
// //   HAL_DMA_IRQHandler(&hdma_usart2_tx);
// //   /* USER CODE BEGIN DMA1_Stream6_IRQn 1 */
// //   usart_on_dma_interrupt(DMA1_Stream6);

// //   /* USER CODE END DMA1_Stream6_IRQn 1 */
// // }



























/* Includes ------------------------------------------------------------------*/


void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */

  /* USER CODE END SysTick_IRQn 0 */
  HAL_IncTick();
  /* USER CODE BEGIN SysTick_IRQn 1 */

  /* USER CODE END SysTick_IRQn 1 */
}

void DMA1_Stream2_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Stream1_IRQn 0 */

  /* USER CODE END DMA1_Stream1_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_uart4_rx);
  /* USER CODE BEGIN DMA1_Stream1_IRQn 1 */

  /* USER CODE END DMA1_Stream1_IRQn 1 */
}

void DMA1_Stream4_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Stream1_IRQn 0 */

  /* USER CODE END DMA1_Stream1_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_uart4_tx);
  /* USER CODE BEGIN DMA1_Stream1_IRQn 1 */

  /* USER CODE END DMA1_Stream1_IRQn 1 */
}

void DMA1_Stream1_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Stream1_IRQn 0 */

  /* USER CODE END DMA1_Stream1_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_usart3_rx);
  /* USER CODE BEGIN DMA1_Stream1_IRQn 1 */

  /* USER CODE END DMA1_Stream1_IRQn 1 */
}

/**
  * @brief This function handles DMA1 stream3 global interrupt.
  */
// void DMA1_Stream3_IRQHandler(void)
// {
//   /* USER CODE BEGIN DMA1_Stream3_IRQn 0 */

//   /* USER CODE END DMA1_Stream3_IRQn 0 */
//   HAL_DMA_IRQHandler(&hdma_usart3_tx);
//   /* USER CODE BEGIN DMA1_Stream3_IRQn 1 */

//   /* USER CODE END DMA1_Stream3_IRQn 1 */
// }

/**
  * @brief This function handles DMA1 stream5 global interrupt.
  */
void DMA1_Stream5_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Stream5_IRQn 0 */

  /* USER CODE END DMA1_Stream5_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_usart2_rx);
  /* USER CODE BEGIN DMA1_Stream5_IRQn 1 */

  /* USER CODE END DMA1_Stream5_IRQn 1 */
}

/**
  * @brief This function handles DMA1 stream6 global interrupt.
  */
void DMA1_Stream6_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Stream6_IRQn 0 */

  /* USER CODE END DMA1_Stream6_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_usart2_tx);
  /* USER CODE BEGIN DMA1_Stream6_IRQn 1 */

  /* USER CODE END DMA1_Stream6_IRQn 1 */
}

/**
  * @brief This function handles DMA2 stream1 global interrupt.
  */
void DMA2_Stream1_IRQHandler(void)
{
  /* USER CODE BEGIN DMA2_Stream1_IRQn 0 */

  /* USER CODE END DMA2_Stream1_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_usart6_rx);
  /* USER CODE BEGIN DMA2_Stream1_IRQn 1 */

  /* USER CODE END DMA2_Stream1_IRQn 1 */
}

/**
  * @brief This function handles DMA2 stream3 global interrupt.
  */
// void DMA2_Stream3_IRQHandler(void)
// {
//   /* USER CODE BEGIN DMA2_Stream3_IRQn 0 */

//   /* USER CODE END DMA2_Stream3_IRQn 0 */
//   HAL_DMA_IRQHandler(&hdma_sdio);
//   /* USER CODE BEGIN DMA2_Stream3_IRQn 1 */

//   /* USER CODE END DMA2_Stream3_IRQn 1 */
// }

/**
  * @brief This function handles DMA2 stream6 global interrupt.
  */
void DMA2_Stream6_IRQHandler(void)
{
  /* USER CODE BEGIN DMA2_Stream6_IRQn 0 */

  /* USER CODE END DMA2_Stream6_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_usart6_tx);
  /* USER CODE BEGIN DMA2_Stream6_IRQn 1 */

  /* USER CODE END DMA2_Stream6_IRQn 1 */
}

/**
  * @brief This function handles DMA2 stream7 global interrupt.
  */
// void DMA2_Stream7_IRQHandler(void)
// {
//   /* USER CODE BEGIN DMA2_Stream7_IRQn 0 */

//   /* USER CODE END DMA2_Stream7_IRQn 0 */
//   HAL_DMA_IRQHandler(&hdma_usart1_tx);
//   /* USER CODE BEGIN DMA2_Stream7_IRQn 1 */

//   /* USER CODE END DMA2_Stream7_IRQn 1 */
// }

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
