#ifndef __SADKO__
#define __SADKO__ 1

#ifndef UNITTEST
	#include "usart_port.h"
	#include "modbus.h"
	#include "errors.h"
	#include "input.h"
	#include "CRC.h"
    #include "ff.h"
	#include "soft_timer.h"

	#define SADKO_MODBUS_ADDR 0xE
    #define SADKO_RESPONSE_TIMEOUT 15 // 174 // ms

	// from port
	void sadko_on_message(usart_port_t* port);

	void sadko_set_modbus_addr(uint8_t addr);
	void sadko_task(void);
	int sadko_update(void);

	int sadko_init(usart_port_t* port, uint8_t addr);

	void sadko_set_output_index(uint32_t id);
	void sadko_data(char* string);
#else
	#include <stdint.h>
	#include <stdbool.h>
#endif

#define CMD_APPROVE 1

typedef struct
{
#ifndef UNITTEST
    usart_port_t *uport;
    FIL      file;
    FILINFO  fno;
    soft_timer_t* timer;
#endif
    char     filename[64];
    uint32_t file_read_ptr; // текущая позиция чтение
    uint32_t file_packets;
    uint32_t file_packets_sended;

    uint32_t approved_count; // устанавливается в прерывании - обрабатывается в основном цикле

    bool     on_message_flag;
    uint8_t*  modbus_data;
    uint16_t  modbus_data_len;

    bool     is_file_opened;
    bool     is_config_read;
    bool     update_rtc;
    // храним считанное сообщение - так как длина перед чтением неизвестна и оно может не поместиться в оставшееся свободное место в буфере
    uint8_t  message[255];
    uint16_t message_length;

    bool     output_ready;      // если true -> можно отправлять в садок содержимое output_buffer
    uint8_t  output_buffer[256]; // этот буффер для отправки
    int16_t  output_length;      // длина подготовленных к отправке данных, байт;
    uint8_t  messages_in_outbuffer;

    // если придет подтверждение в виде команды 1 в регистр 0x1001 - инкрементировать счетчик
    uint16_t input_regs1[32];   // 0x1000

    uint16_t holding_regs1[32]; // 0x1000
    uint16_t holding_regs2[32]; // 0x2000
    uint16_t holding_regs8[32]; // 0x8000

    int32_t coeffs[3]; // коэффициенты сварки
}sadko_t;

typedef struct
{
    uint32_t read_ptr; // позиция с которой продолжать чтение после выключения
    uint32_t packet_sended; // количество отданных пакетов
    char     filename[64];
    // а имя файла хранится в file_list
}sadko_settings_t;

void sadko_update_status(void);
void sadko_reset_coeffs(void);
void sadko_update_rtc(void);
void sadko_send_complete(void);

#endif
