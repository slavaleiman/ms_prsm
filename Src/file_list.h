#ifndef _FILE_LIST_H_
#define _FILE_LIST_H_ 1
#include "ff.h"

typedef struct
{
	uint32_t count; // количество записей
    FIL 	 file;
}file_list_t;

int file_list_push(char* filename); // записать имя файла
int file_list_peek(char* filename, int maxlen); // взять имя файла, не удаляя из списка
int file_list_pop(void); // удалить верхнее имя файла из списка
int file_list_init(void);

#endif
