#include "list.h"
#include "stdlib.h"

list_t* list_init(void)
{
    list_t *list = malloc(sizeof(list_t));
    list->first = NULL;
    list->size = 0;
    return list;
}

void list_destroy(list_t *list)
{
    free(list);
}

inline void list_first(list_t *list, item_t** item)
{
    *item = list->first;
}

uint8_t list_next(list_t *list, item_t** item)
{
    if(*item)
    {
        *item = (*item)->next;
        return *item ? 1 : 0;
    }
    return 0;
}

// inline int list_is_data(list_t *list, item_t** item)
// {
//     return (*item != NULL);
// }

inline void * list_data(list_t *list, item_t** item)
{
    if(*item != NULL)
        return (*item)->data;
    else
        return NULL;
}

inline size_t list_size(list_t *list)
{
    return list->size;
}

void list_insert_head(list_t *list, void *data)
{
    item_t *item = malloc(sizeof(item_t));
    item->data = data;
    item->next = NULL;
    if(!list->size)
    {
        list->first = item;
    }else{
        item->next = list->first;
        list->first = item;
    }
    ++list->size;
}

void list_insert_tail(list_t *list, void *data)
{
    item_t *item = malloc(sizeof(item_t));
    item->data = data;
    item->next = NULL;
    if(!list->size)
    {
        list->first = item;
    }else if(list->last)
    {
        item_t* sub_last = list->last;
        sub_last->next = item;
    }
    list->last = item;
    ++list->size;
}

// DO NOT USE inside list_for macro, list->current = NULL;
void list_remove_item(list_t *list, void* data)
{
    //do not use lists current here
	if(!data)
        return;
    if(!list->size)
        return;
    if(list->first == NULL)
        return;
    item_t* prev = NULL;

    for(item_t* current = list->first; current != NULL; current = current->next)
    {
        if(current->data == data)
        {
            item_t* next = current->next;
            --list->size;
            if(prev)
                prev->next = next;
            if(list->first == current)
                list->first = next;
            if(list->last == current)
                list->last = prev;
            free(current/*, sizeof(item_t)*/);
            break;
        }
        prev = current;
    }
}

void list_prev(list_t *list, item_t** item)
{
    if(!*item)
        return;
    if(!list->size)
        return;
    if(list->first == NULL)
        return;
    item_t* prev = NULL;

    for(item_t* current = list->first; current != NULL; current = current->next)
    {
        if(current == *item)
        {
            break;
        }
        prev = current;
    }
    *item = prev; // can be NULL
}
