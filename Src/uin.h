#ifndef __UIN__
#define __UIN__ 1
#include <stdint.h>
#include "usart_port.h"

void uin_on_message(usart_port_t* port);
void uin_init(usart_port_t* port, uint8_t modbus_addr);

#define UIN_MODBUS_ADDR 0xA

#endif
