#include <stdint.h>
#include <string.h>
#include "input.h"
#include "errors.h"
#include "log_sd.h"
#include <stdio.h>
#include "circular_buffer.h"
#include "uptime.h"
#include "list.h"
#include "packet_types.h"
#include "input_selector.h"
#ifndef UNITTEST
	#include "sadko.h"
	#include "stm32f4xx_hal.h"
#endif
#include "unixtime.h"

extern usart_port_t uport1; // input 232
extern usart_port_t uport2; // input 485

uint64_t input_timestamp;

typedef struct 
{
	int channel;
	int from;
	int to;
}range_t;

list_t* input_ranges; // for filter

extern circular_buffer_t input_cbuffer;

void input_update_timestamp(void)
{
	input_timestamp = unixtime();
}

void input_push_data(usart_port_t* port, uint8_t* data, uint16_t len)
{
	++port->msg_count;
	// push to input_cbuffer
	uint8_t id_data[256 + 16];

	uint16_t data_length = len + INPUT_MSG_HEADER_LEN - 2; // тут длина хранится в байтах
	uint16_t mess_id = port->msg_count % 0xFFFF;
	uint16_t mess_type = (port == &uport1) ? UIN_PACKET : KAHOVKA_PACKET;
	uint64_t timestamp = input_timestamp;

	id_data[0] = data_length >> 8;
	id_data[1] = data_length & 0xFF;
	id_data[2] = mess_id >> 8;
	id_data[3] = mess_id & 0xFF;
	id_data[4] = mess_type >> 8;
	id_data[5] = mess_type & 0xFF;

	id_data[6] = (timestamp >> 8) & 0xFF;
	id_data[7] = timestamp & 0xFF;
	id_data[8] = (timestamp >> 24) & 0xFF;
	id_data[9] = (timestamp >> 16) & 0xFF;

	id_data[10] = (timestamp >> 40) & 0xFF;
	id_data[11] = (timestamp >> 32) & 0xFF;
	id_data[12] = (timestamp >> 56) & 0xFF;
	id_data[13] = (timestamp >> 48) & 0xFF;

	memcpy(&id_data[INPUT_MSG_HEADER_LEN], data, len);

#ifdef UNITTEST
	printf("l = 0x%x len = 0x%x\n", l, len);
	memdump(id_data, len + l, 16);
#else
	int ret = cbuffer_push_message(&input_cbuffer, id_data, len + INPUT_MSG_HEADER_LEN);
	if(ret < 0)
		errors_on_error(CBUFFER_PUSH_FAIL);
#endif
}

// выполняется в прерывании 
void input_on_message(usart_port_t* port)
{
    uint8_t* data 	= port->rx_buffer;
    uint16_t len 	= port->rx_size;

    // это вход данных со сварки
    // маслобак включен (0 = вкл - вход инвертирован)
	// if(0 == selector_current_input())
	// {
    input_push_data(port, data, len);
	// }
}

// check changes in all ranges
// int input_filter_data(input_t* input, uint8_t* data, int len)
// {
//     if(len <= 0)
//         return 0;
//     uint16_t i = 0;

//     if(!input_ranges || !list_size(input_ranges))
//     {
//     	return 1;
//     }

//     item_t* item;
// 	list_first(input_ranges, &item);
// 	do
//     {
//     	range_t *r = list_data(input_ranges, &item);
//     	if(r)
//     	{
//     		if(r->from > len)
//     		{
//     			return 1; // если сообщение короче диапазона фильтра - то оно проходит
//     		}
// 		    for(i = r->from; (i < len) && (i <= r->to); ++i)
// 		    {
// 		        if(data[i] != input->buffer[i])
// 		            return 1;
// 		    }
//     	}
//     }while(list_next(input_ranges, &item));
//     return 0; // no changes
// }

#ifndef UNITTEST
// для LCD
void input_data(char* string)
{
	// input_t* input = &input1;
	char* filename;
	log_filename(&filename);
	char name[] = "             ";
	strcpy(name, filename);
	sprintf(string, "message received:%ld\n"
					// "message saved:   %ld\n"
					"log file: %s\n"
					"write position: %4ld\n"
					"in buffer count: %4d",
	    				 // port->msg_count,
	    				 log_read_index(),
	    				 filename,
						 log_current_file_position(),
	    				 input_cbuffer.count
				 );
}
#endif

void find_ranges(FIL* file)
{
	char line[255];
	do{
		char* msg = f_gets((TCHAR*)&line, 255, file);
		if(!msg)
			return;
		char pattern[] = "[ranges]";
		char* ranges = strstr(msg, pattern);
		if(ranges)
		{
			do{
				msg = f_gets((TCHAR*)&line, 255, file); // проверь что чтение пошло дальше
				if(!msg)
					break;
				char* range = strstr(msg, "(");
				if(!range)
					continue;
				int channel = atoi((char*)&range[1]);
				char* echan = strstr(msg, ":");
				if(!echan)
					continue;
				int from = atoi((char*)&echan[1]);

				char* _to = strstr(msg, ",");
				if(!_to)
					continue;
				int to = atoi((char*)&_to[1]);

				range_t r = {.channel = channel, .from = from, .to = to};

				list_insert_tail(input_ranges, &r);
			}while(1);
		}
	}while(1);
}

int32_t coeffs[3];

void find_welding_coeff(FIL* file)
{
	char line[255];
	do{
		char* msg = f_gets((char*)&line, 255, file);
		if(!msg)
			return;
		char pattern[] = "[welding_coeff]";
		char* coeffs_pattern = strstr(msg, pattern);
		if(coeffs_pattern)
		{
			unsigned i = 0;
			do{
				++i;
				msg = f_gets((char*)&line, 255, file); // проверь что чтение пошло дальше
				if(!msg)
					break;
				coeffs[0] = atoi((char*)msg);
				
				char* sec_substr = strstr(msg, ",");
				if(!sec_substr)
					continue;
				coeffs[1] = atoi((char*)&sec_substr[1]);

				char* thrird_substr = strstr(&sec_substr[1], ",");
				if(!thrird_substr)
					continue;
				coeffs[2] = atoi((char*)&thrird_substr[1]);

				if(coeffs[0] && coeffs[1] && coeffs[2])
					return;
			}while(1);
		}
	}while(1);
}

void input_welding_coeffs(int32_t* values)
{
	memcpy(values, coeffs, sizeof(coeffs));
}

void inputs_configure(void)
{
	FIL file;
    FRESULT res = f_open(&file, ".input_config", FA_READ);
	if(res != FR_OK) return;
	find_ranges(&file);
	f_rewind(&file);
	find_welding_coeff(&file);
	f_close(&file);
}
