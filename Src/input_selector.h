#ifndef INPUT_SELECTOR_H
#define INPUT_SELECTOR_H 1

#include "main.h"

#define SELECTOR_PORT 	D1_PORT // 
#define SELECTOR_PIN 	D1_PIN  //

int selector_current_input(void);

#endif
