#ifndef LED_H
#define LED_H 1

#define REC_LED 	GPIO_PIN_14 // RECORD ACTIVE GREEN // карта в норме
#define OUTPUT_LED 	GPIO_PIN_15 // OUTPUT ACTIVE BLUE  // идет обмен с садко

void led_on(uint16_t LED);
void led_off(uint16_t LED);
void led_switch(uint16_t LED);
void led_init(void);
void led_task(void);

#endif
