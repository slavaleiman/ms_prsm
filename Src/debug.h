#ifndef _DEBUG_H_
#define _DEBUG_H_ 1

#define CYCLES_COUNT_FROM(TIMER_NAME)\
    uint32_t t1 = DWT->CYCCNT;\
    uint32_t TIMER_NAME = 0;

#define CYCLES_COUNT_TO(TIMER_NAME) \
    uint32_t t2 = DWT->CYCCNT;		\
    TIMER_NAME = t2 - t1; 			\
    if(TIMER_NAME)__NOP();
#endif //_DEBUG_H_