#include <stddef.h>
#include <stdlib.h>
#include "stm32f4xx.h"
#include "stm32f4xx_hal_tim.h"

#include "list.h"
#include "soft_timer.h"

list_t* soft_timers = NULL;
TIM_HandleTypeDef htim2;

void soft_timer_init(void)
{
    if(!soft_timers)
    {
        soft_timers = list_init();
    }

    __HAL_RCC_TIM2_CLK_ENABLE();

    uint32_t uwTimclock = HAL_RCC_GetPCLK2Freq();
    uint32_t uwPrescalerValue = (uint32_t) ((uwTimclock / 1000) - 1);

    htim2.Instance = TIM2;
    htim2.Init.Period = 5 - 1;
    htim2.Init.Prescaler = uwPrescalerValue;
    htim2.Init.ClockDivision = 0;
    htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
    if(HAL_TIM_Base_Init(&htim2) == HAL_OK)
    {
       HAL_TIM_Base_Start_IT(&htim2);
    }
    NVIC_SetPriority(TIM2_IRQn, 6);
    NVIC_EnableIRQ(TIM2_IRQn);
}

void TIM2_IRQHandler(void)
{
  TIM2->SR &= ~TIM_SR_UIF;
  soft_timer_tick();
}

soft_timer_t* soft_timer(uint32_t period, void (*cb)(void), bool is_one_shot, bool do_remove)
{
    if(!soft_timers)
    {
        soft_timer_init();
    }

    soft_timer_t* soft_timer = malloc(sizeof(soft_timer_t));
    soft_timer->cb = cb;
    soft_timer->one_shot = is_one_shot;
    soft_timer->period = period;
    soft_timer->count = period + 1;
    soft_timer->do_remove = do_remove;
#ifdef UNITTEST        
        printf("%s soft timers\n", __FUNCTION__);
#endif    
    list_insert_tail(soft_timers, soft_timer);
    soft_timer->is_active = true;
    return soft_timer;
}

void soft_timer_stop(soft_timer_t* soft_timer)
{
    if(soft_timer)
    {
        soft_timer->is_active = false;
    }
}

void soft_timer_restart(soft_timer_t* soft_timer)
{
    if(soft_timer)
    {
        soft_timer->count = soft_timer->period + 1;
        soft_timer->is_active = true;
    }
}

bool soft_timer_is_active(soft_timer_t* soft_timer)
{
    return soft_timer->is_active;
}

void soft_timer_tick(void)
{
    item_t* item = NULL;
    list_first(soft_timers, &item);
    do
    {
        soft_timer_t* t = list_data(soft_timers, &item);
        if(!t)
            break;
        if(t->is_active && t->count)
            --t->count;
    }while(list_next(soft_timers, &item));
}

void soft_timer_poll(void)
{
    if(!soft_timers)
    {
        return;
    }

    uint8_t toremove = 0;
    item_t* item = NULL;
    list_first(soft_timers, &item);
    do
    {
        soft_timer_t* t = list_data(soft_timers, &item);
        if(t)
        {
            if(t->is_active)
            {
                if(t->count == 0)
                {
                    if(t->one_shot)
                    {
                        t->is_active = false;
                        ++toremove;
                    }else{
                        t->count = t->period;
                    }
                    t->cb();
                }
            }
            else
            {
                if(t->do_remove)
                    ++toremove;
            }
        }
    }while(list_next(soft_timers, &item));
    if(toremove)
    {
        list_first(soft_timers, &item);
        do
        {
            soft_timer_t* soft_timer = list_data(soft_timers, &item);
            if((!soft_timer->is_active) && soft_timer->do_remove)
            {
                list_remove_item(soft_timers, soft_timer);
                return;
            }
        }while(list_next(soft_timers, &item));
    }
}
