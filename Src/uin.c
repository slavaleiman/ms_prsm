#include "main.h"
#include "uin.h"
#include "input.h"
#include "input_selector.h"

// этот модуль по сути - modbus_device
// хранит состояние и отдает всегда одинаковые пакеты

#define UIN_MODBUS_OFFSET 0xA000
// #define MODBUS_REQ_HEADER_LEN 6 // addr + func + a1 + a2 + q1 + q2 = 6  
#define MODBUS_RESPONSE_HEADER_LEN 3 // addr + func + len_bytes
struct
{
    bool on_request_flag;

    usart_port_t* port;
    
    uint16_t req_addr;
    uint16_t req_nums_items;
	uint8_t  request[8];

	uint8_t output_buffer[34]; // такой размер обусловлен имеющимся протоколом
}uin_device;

// сюда доходит только пакет - ответ на запрос мастера
// это могут быть все регистры, а может быть только часть
// запрошенный адрес != 0
void uin_process_response(uint8_t* message, uint16_t len)
{
    uint16_t output_buffer_size = sizeof(uin_device.output_buffer);

    if(uin_device.req_addr < UIN_MODBUS_OFFSET)
        return;
    uint16_t offset    = (uin_device.req_addr - UIN_MODBUS_OFFSET) * 2; // адрес регистра
    uint16_t num_bytes = uin_device.req_nums_items * 2;
    if(offset > output_buffer_size)
        return;
    if((offset + num_bytes) > output_buffer_size)
        num_bytes = output_buffer_size - offset;

    // нужно сложить данные в нужные регистры 
    memcpy(&uin_device.output_buffer[offset], &message[MODBUS_RESPONSE_HEADER_LEN], num_bytes);    
    // а отправить полный пакет состояния
    input_push_data(uin_device.port, uin_device.output_buffer, sizeof(uin_device.output_buffer));
}

int check_length(uint8_t* message, uint16_t length)
{
    uint8_t len = message[2];
    if(length == len + 5) // addr + func + num_byte + crc = 5
        return 1;
    return 0;
}

void uin_process_message(void)
{
    uint8_t* message = uin_device.port->rx_buffer;
	uint16_t size 	 = uin_device.port->rx_size;

    const uint8_t addr = message[0];
    const uint8_t func = message[1];
    if(addr != uin_device.port->addr)
        return;
    if(size == 8) // request
    {
        if((func == 3) || (func == 4))
        {
            memcpy(uin_device.request, message, size);
            uin_device.on_request_flag = true;
            uin_device.req_addr = message[2] << 8 | message[3];
            uin_device.req_nums_items = message[4] << 8 | message[5];
        }
    }else{
        if(uin_device.on_request_flag)
        {
            if(uin_device.req_addr && uin_device.req_nums_items)
            {
                if(check_length(message, size))
                {
                    if((func == 3) || (func == 4))
                    {
                        uin_process_response(message, size);
                    }
                }
            }
            uin_device.on_request_flag = false;
            uin_device.req_addr = 0;
            uin_device.req_nums_items = 0;
        }
    }
}

void uin_on_message(usart_port_t* port)
{
    // если маслобак выключен - сохраняем сообщения
    // ВХОДЫ DI инвертированы
    if(1 == selector_current_input())
    {
        uin_process_message();
    }
}

void uin_init(usart_port_t* port, uint8_t modbus_addr)
{
    uin_device.port = port;
    uin_device.port->addr = modbus_addr;
    memset(uin_device.output_buffer, 0, sizeof(uin_device.output_buffer));
    HAL_GPIO_WritePin(UIN_ENABLE_PORT, UIN_ENABLE_PIN, 1); // DE=1 driver disable
}
