#include "main.h"
#include "usart_port.h"
#ifndef UNITTEST
#include <stdbool.h>
#endif
#include "debug.h"
#include "modbus.h"
#include "errors.h"
#include "debug.h"
#include "circular_buffer.h"
#include "sadko.h"

extern usart_port_t uport1; // input 232
extern usart_port_t uport2; // input 485
extern usart_port_t uport3; // output 485 sadko

void uport_transmit_dma(usart_port_t* uport, uint8_t* msg, uint16_t size);

void uport_transmit(usart_port_t* uport)
{
#ifndef UNITTEST
    uport_transmit_dma(uport, uport->tx_buffer, uport->tx_size);
    uport->is_sending = true;
#endif
}

int uport_process(usart_port_t* uport)
{
    if(!uport)
        return 0;
    if(uport->rx_size > 0)
    {
        if(uport->on_message)
        {
            uport->on_message(uport);
        }
        uport->rx_size = 0;
        return 1;
    }
    return 0;
}

// #define CHECK_TX 0

#ifdef CHECK_TX
static void check_trasmition(usart_port_t* uport, size_t len)
{
    for(uint8_t i = 0; i < len; ++i)
    {
        if(uport->dma_rx_buffer[i] ^ uport->tx_buffer[i])
        {
            // modbus_inc_send_err();
            errors_on_error(MODBUS_TX_ERROR);
            return;
        }
    }
}
#endif

// debug
uint32_t push_time_diff = 0;
uint32_t msg_count = 0;

void usart_irq_handler(usart_port_t *port)
{
    if(!port->is_init)
    {
        port->usart->SR &= ~(USART_SR_ORE | USART_SR_FE | USART_SR_NE);
        return;
    }
    uint32_t sr = port->usart->SR;
    if(sr & USART_SR_IDLE)
    {
        (void)port->usart->DR;
        
        port->usart->SR &= ~(USART_SR_IDLE);
        size_t len = DMA_RX_BUFFER_SIZE - port->dma_input_stream->NDTR;
        if(len)
        {
#if 0 // CHECK_TX
            if(port->is_sending == true) // отправленное сообщение
            {
                port->is_sending = false;
                HAL_GPIO_WritePin(SADKO_ENABLE_PORT, SADKO_ENABLE_PIN, 1); // DE=0 driver disable 
                // port->dma_input_stream->CR &= ~DMA_SxCR_EN;
                // check_trasmition(port, len);
                // memset(port->dma_rx_buffer, 0, len);
                // port->dma_input_stream->M0AR = (uint32_t)port->dma_rx_buffer; /* Set memory address for DMA again */
                // port->dma_input_stream->NDTR = DMA_RX_BUFFER_SIZE;     /* Set number of bytes to receive */
                // port->dma_input_stream->CR |= DMA_SxCR_EN;              /* Start DMA transfer */
            }
            else
#endif
            {
                port->dma_input_stream->CR &= ~DMA_SxCR_EN;
                memcpy(port->rx_buffer, port->dma_rx_buffer, len);
                port->rx_size = len;
                memset(port->dma_rx_buffer, 0, DMA_RX_BUFFER_SIZE);
                
                uport_process(port);

                ++msg_count;
                port->dma_input_stream->M0AR = (uint32_t)port->dma_rx_buffer; /* Set memory address for DMA again */
                port->dma_input_stream->NDTR = DMA_RX_BUFFER_SIZE;     /* Set number of bytes to receive */
                port->dma_input_stream->CR |= DMA_SxCR_EN;              /* Start DMA transfer */
            }
        }
    }
    if((sr & USART_SR_TC) && (USART3->CR1 & USART_CR1_TCIE))
    {
        USART3->SR &= ~USART_SR_TC;
        sadko_send_complete();
        USART3->CR1 &= ~USART_CR1_TCIE;
        port->dma_input_stream->CR &= ~DMA_SxCR_EN;
        // memset(port->dma_rx_buffer, 0, len);
        port->dma_input_stream->M0AR = (uint32_t)port->dma_rx_buffer; /* Set memory address for DMA again */
        port->dma_input_stream->NDTR = DMA_RX_BUFFER_SIZE;     /* Set number of bytes to receive */
        uport3.dma_input_stream->CR |= DMA_SxCR_TCIE | DMA_SxCR_TEIE | DMA_SxCR_DMEIE;
        port->dma_input_stream->CR |= DMA_SxCR_EN;              /* Start DMA transfer */
    }
    if(sr & USART_SR_ORE)
    {
        // port->usart->ICR |= USART_ICR_ORECF;
        port->usart->SR &= ~USART_SR_ORE;
        // (void)port->usart->RDR;
        (void)port->usart->DR;
        port->usart->CR1 &= ~USART_CR1_UE;
        port->usart->CR3 |= USART_CR3_DMAT | USART_CR3_DMAR;
        port->usart->CR1 |= USART_CR1_UE;
        // reinit_dma
        // memset(port->dma_rx_buffer, 0, DMA_RX_BUFFER_SIZE);
        port->dma_input_stream->CR &= ~DMA_SxCR_EN;
        port->dma_input_stream->M0AR = (uint32_t)port->dma_rx_buffer; /* Set memory address for DMA again */
        port->dma_input_stream->NDTR = DMA_RX_BUFFER_SIZE;     /* Set number of bytes to receive */
        port->dma_input_stream->CR |= DMA_SxCR_TCIE | DMA_SxCR_TEIE;
        port->dma_input_stream->CR |= DMA_SxCR_EN;              /* Start DMA transfer */
        errors_on_error(RS485_OVERRUN_ERROR); // without led
        if(port == &uport3) // sadko
        {
            HAL_GPIO_WritePin(SADKO_ENABLE_PORT, SADKO_ENABLE_PIN, 1); // DE=0 driver disable
        }
    }
}

#ifndef UNITTEST
void uport_gpio_init(usart_port_t *port)
{
    // INIT IN MSP
}

extern usart_port_t uport3;

void usart_gpio_init(usart_port_t* port)
{
    // INIT IN MSP
}

// AUTO DRIVER CONTROL 
// void uport_driver_control_reset(usart_port_t* port)
// {
    // if(port->type == 485)
	// if(port->de_port && port->de_pin)
	// {
	//     port->de_port->BSRR = (uint32_t)port->de_pin << 16; // bit reset
	// }
// }

static void dma_config(usart_port_t* port)
{
    port->dma_input_stream->CR  |= DMA_SxCR_TCIE | DMA_SxCR_TEIE | DMA_SxCR_DMEIE;
    port->dma_input_stream->FCR |= 0x00000080U;

    port->dma_input_stream->CR &= (uint32_t)(~DMA_SxCR_DBM);

    /* Configure DMA Stream data length */
    port->dma_input_stream->NDTR = DMA_RX_BUFFER_SIZE;
    /* Peripheral to Memory */
    /* Configure DMA Stream source address */
    port->dma_input_stream->PAR = (uint32_t)&port->usart->DR;
    /* Configure DMA Stream destination address */
    port->dma_input_stream->M0AR = (uint32_t)port->dma_rx_buffer;

    port->dma_input_stream->CR |= port->dma_input_stream_channel 
                                | DMA_SxCR_MINC 
                                | DMA_SxCR_CIRC; 
    // may be calc baseregister DMA

    // TX Init
    port->dma_output_stream->CR |= port->dma_output_stream_channel
                                | DMA_SxCR_MINC;

    port->dma_output_stream->FCR |= 0x00000080U; 
}

void usart_init(usart_port_t* port)
{
    port->usart->CR1 &= ~USART_CR1_UE;

    // usart_gpio_init(port);
    // set idle
    port->usart->CR1 |= USART_CR1_TE | USART_CR1_RE | USART_CR1_IDLEIE; //  это если есть проверка отправки

    // port->usart->CR1 &= ~USART_CR1_TE;

    // port->usart->CR1 |= USART_CR1_TE | USART_CR1_RE | USART_CR1_IDLEIE | USART_CR1_OVER8;
    // uint32_t usartdiv = (SystemCoreClock + port->baudrate / 2) / port->baudrate;
    // uint16_t brrtemp = (uint16_t)(usartdiv & 0xFFF0U);
    // brrtemp |= (uint16_t)((usartdiv & (uint16_t)0x000FU) >> 1U);
    // port->usart->BRR = brrtemp;

    port->usart->CR3 |= USART_CR3_DMAT | USART_CR3_DMAR;
    port->usart->CR1 |= USART_CR1_UE;

    // uport_driver_control_reset(port); // AUTO

    port->is_sending = false;

    dma_config(port);
}

static void dma_transmit(usart_port_t* uport, uint32_t mem_address, uint32_t periph_address, uint32_t DataLength)
{
    DMA_Stream_TypeDef* dma_stream = uport->dma_output_stream;
    uint32_t stream_number = (((uint32_t)dma_stream & 0xFFU) - 16U) / 24U;

    /* lookup table for necessary bitshift of flags within status registers */
    static const uint8_t flagBitshiftOffset[8U] = {0U, 6U, 16U, 22U, 0U, 6U, 16U, 22U};
    uint8_t stream_index = flagBitshiftOffset[stream_number];
    uint32_t stream_base;
    if(stream_number > 3U)
    {
    /* return pointer to HISR and HIFCR */
        stream_base = (((uint32_t)dma_stream & (uint32_t)(~0x3FFU)) + 4U);
    }else{
    /* return pointer to LISR and LIFCR */
        stream_base = ((uint32_t)dma_stream & (uint32_t)(~0x3FFU));
    }

    typedef struct
    {
      __IO uint32_t ISR;   /*!< DMA interrupt status register */
      __IO uint32_t Reserved0;
      __IO uint32_t IFCR;  /*!< DMA interrupt flag clear register */
    } DMA_Base_Registers;
    
    DMA_Base_Registers *regs = (DMA_Base_Registers *)(stream_base & (uint32_t)(~0x3FFU));
    /* Configure the source, destination address and the data length */
    
    /* Clear DBM bit */
    dma_stream->CR &=  ~DMA_SxCR_EN;
    dma_stream->CR |= uport->dma_output_stream_channel |
                        DMA_SxCR_MINC |     // DMA_MINC_ENABLE
                        DMA_SxCR_PL_1 |     // Priority level: High
                        DMA_SxCR_DIR_0;     // MEM TO PERIPH
                        // DMA_SxCR_PSIZE |  // DMA_PDATAALIGN_BYTE
                        // DMA_SxCR_MSIZE_|  // DMA_MDATAALIGN_BYTE
                        // DMA_SxCR_CIRC |     // Circular mode 
                        // DMA_FIFOMODE_DISABLE
                        // DMA_PINC_DISABLE;

    /* Configure DMA Stream data length */
    dma_stream->NDTR = DataLength;

    /* Peripheral to Memory */
    /* Configure DMA Stream destination address */
    dma_stream->PAR = periph_address;

    /* Configure DMA Stream source address */
    dma_stream->M0AR = mem_address;

    /* Clear all interrupt flags at correct offset within the register */
    regs->IFCR = 0x3FU << stream_index;
    
    /* Enable Common interrupts*/
    dma_stream->CR  |= DMA_SxCR_DMEIE | DMA_SxCR_TCIE | DMA_SxCR_TEIE;

    /* Enable the Peripheral */
    dma_stream->CR |= DMA_SxCR_EN;
}

void usart_on_dma_interrupt(DMA_Stream_TypeDef* dma_stream)
{
    // PORT1
    // TX 
    if(DMA2->HISR & DMA_HISR_TEIF7)
    {
        DMA2->HIFCR |= DMA_HIFCR_CTEIF7;
    }
    if(DMA2->HISR & DMA_HISR_FEIF7)
    {
        DMA2->HIFCR |= DMA_HIFCR_CFEIF7;
    }
    if(DMA2->HISR & DMA_HISR_TCIF7)
    {
        DMA2->HIFCR |= DMA_HIFCR_CTCIF7;
    }
    if(DMA2->HISR & DMA_HISR_HTIF7)
    {
        DMA2->HIFCR |= DMA_HIFCR_CHTIF7;
    }
    // RX
    if(DMA2->LISR & DMA_LISR_TEIF2)
    {
        DMA2->LIFCR |= DMA_LIFCR_CTEIF2;
    }
    if(DMA2->LISR & DMA_LISR_FEIF2)
    {
        DMA2->LIFCR |= DMA_LIFCR_CFEIF2; // no fifo
    }
    if(DMA2->LISR & DMA_LISR_TCIF2)
    {
        DMA2->LIFCR |= DMA_LIFCR_CTCIF2;
    }
    if(DMA2->LISR & DMA_LISR_HTIF2)
    {
        DMA2->LIFCR |= DMA_LIFCR_CHTIF2;
    }        
// -----------------------------------------
    // no half transfer
    // TX
    if(DMA1->LISR & DMA_LISR_TEIF3)
    {
        DMA1->LIFCR |= DMA_LIFCR_CTEIF3;
    }
    if(DMA1->LISR & DMA_LISR_FEIF3)
    {
        DMA1->LIFCR |= DMA_LIFCR_CFEIF3;
    }
    if(DMA1->LISR & DMA_LISR_TCIF3)
    {
        DMA1->LIFCR |= DMA_LIFCR_CTCIF3;
        USART3->CR1 |= USART_CR1_TCIE;
        uport3.dma_input_stream->CR &= ~(DMA_SxCR_TCIE | DMA_SxCR_TEIE | DMA_SxCR_DMEIE);
    }
    // RX
    if(DMA1->LISR & DMA_LISR_TEIF1)
    {
        DMA1->LIFCR |= DMA_LIFCR_CTEIF1;
    }
    if(DMA1->LISR & DMA_LISR_FEIF1)
    {
        DMA1->LIFCR |= DMA_LIFCR_CFEIF1; // no fifo
    }
    if(DMA1->LISR & DMA_LISR_TCIF1)
    {
        DMA1->LIFCR |= DMA_LIFCR_CTCIF1;
    }

// -------------------------
    // TX 
    if(DMA1->HISR & DMA_HISR_TEIF4)
    {
        DMA1->HIFCR |= DMA_HIFCR_CTEIF4;
    }
    if(DMA1->HISR & DMA_HISR_FEIF4)
    {
        DMA1->HIFCR |= DMA_HIFCR_CFEIF4;
    }
    if(DMA1->HISR & DMA_HISR_TCIF4)
    {
        DMA1->HIFCR |= DMA_HIFCR_CTCIF4;
    }
    // RX
    if(DMA1->LISR & DMA_LISR_TEIF2)
    {
        DMA1->LIFCR |= DMA_LIFCR_CTEIF2;
    }
    if(DMA1->LISR & DMA_LISR_FEIF2)
    {
        DMA1->LIFCR |= DMA_LIFCR_CFEIF2; // no fifo
    }
    if(DMA1->LISR & DMA_LISR_TCIF2)
    {
        DMA1->LIFCR |= DMA_LIFCR_CTCIF2;
    }
}

void uport_transmit_dma(usart_port_t* uport, uint8_t* msg, uint16_t size)
{
    if(uport->dma_output_stream->NDTR || !msg)
        return;

    dma_transmit(uport, (uint32_t)msg, (uint32_t)&uport->usart->DR, size);
    SET_BIT(uport->usart->CR3, USART_CR3_DMAT);
    CLEAR_BIT(uport->usart->SR, USART_SR_TC);
}

void uport_init(    usart_port_t* uport,
                    USART_TypeDef *USART,
                    DMA_Stream_TypeDef *dma_input_stream,
                    uint32_t dma_input_stream_channel,
                    DMA_Stream_TypeDef *dma_output_stream,
                    uint32_t dma_output_stream_channel,
                    uint32_t baudrate,
                    void (*on_message)(usart_port_t* port)
                    )
{
    uport->is_init = false;
	uport->usart = USART;
    uport->dma_input_stream = dma_input_stream;
    uport->dma_input_stream_channel = dma_input_stream_channel;
    uport->dma_output_stream = dma_output_stream;
    uport->dma_output_stream_channel = dma_output_stream_channel;
    uport->baudrate = baudrate;
    uport->is_sending = false;
    uport->on_message = on_message;

    usart_init(uport);

    memset(uport->dma_rx_buffer, 0, DMA_RX_BUFFER_SIZE);
    uport->dma_input_stream->CR &= ~DMA_SxCR_EN;
    uport->dma_input_stream->M0AR = (uint32_t)uport->dma_rx_buffer; /* Set memory address for DMA again */
    uport->dma_input_stream->NDTR = DMA_RX_BUFFER_SIZE;     /* Set number of bytes to receive */
    uport->dma_input_stream->CR |= DMA_SxCR_TCIE | DMA_SxCR_TEIE | DMA_SxCR_DMEIE;
    uport->dma_input_stream->CR |= DMA_SxCR_EN;              /* Start DMA transfer */
    uport->is_init = true;

    uport->timer = NULL;
}
#endif
