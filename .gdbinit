target extended localhost:4242
set print pretty on
set pagination off

define pt
    p *(soft_timer_t*)(soft_timers->last->data)
end

define rtc
    p/x *RTC
end

define updrtc
    set sadko.holding_regs2[8] = 1
    set sadko.holding_regs2[9] = 0
    set sadko.holding_regs2[10] = 0
    set sadko.holding_regs2[11] = 0
    set sadko.update_rtc = 1
end 

define sadkoinfo
    printf "file: %s\n", sadko.filename
    printf "size:                   %d\n", sadko.fno.fsize
    printf "read ptr:               %d\n", sadko.file_read_ptr
    printf "approved count:         %d\n", sadko.approved_count
    printf "messages_in_outbuffer   %d\n", sadko.messages_in_outbuffer
    printf "message len             %d\n", sadko.message_length
    printf "output_length           %d\n", sadko.output_length
    printf "is opened               %d\n", sadko.is_file_opened
end

define sb
    p/x sadko.message
    p/x sadko.output_buffer
end

#b fill_output_buffer
#b sadko_drop_file
#b sadko_process_output_format
#b sadko.c:473
#b shift_output_buffer

#commands 2 3 4
#    sadkoinfo
#end

#b usart_irq_handler
#b uin_process_response
#b input_push_data
#b sadko.c:394

#b modbus.c:314
#b sadko_read_input

b sadko_send_response
b USART3_IRQHandler
b usart_port.c:320
