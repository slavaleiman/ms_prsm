#!/usr/bin/env node

'use strict';

// Since this is just an example, we'll just read the entire JSON
// file in.

var args = process.argv.slice(2);
console.log('open file: ', args[0]);

// const json = require('./usb1.json');
const json = require(args[0]);
const isFTDI = true;

for (const packet of json)
{
  const layer = packet._source.layers;
  // console.log(packet);

  const frameNum = layer.frame['frame.number'];
  const timeStamp = layer.frame['frame.time_relative'].slice(0, -6);
  const usb = layer.usb;

  // console.log(usb);

  if (layer['usb.capdata'])
  {
    let data = layer['usb.capdata'].replace(/:/g, '');

    const read = usb['usb.dst'] === 'host';
    if (read) {
      if (isFTDI) {
        // Remove the FTDI status bytes
        data = data.slice(4);
      }
      if (data.length == 0) {
        continue;
      }
    }

    let label = `     ${timeStamp}`.slice(-9);
    label += `     ${frameNum}`.slice(-6);
    label += read ? ' R:' : ' W:';

    console.log(label, Buffer.from(data, 'hex'));
  }
}
