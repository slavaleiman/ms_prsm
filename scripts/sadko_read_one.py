#!/usr/bin/python3
# -*- coding: iso-8859-15 -*-

import time
import serial
from modbus import *
import sys
import struct

if len(sys.argv) < 2:
    print("USAGE: %s [port] [baudrate]" % sys.argv[0])
    sys.exit(1)

set_port(sys.argv[1], sys.argv[2])

ADDR = 0xE

def get_msg_size(mb_response):
    # A1  A0  Q1  Q0  N   D (N байт)
    print("response:", mb_response[45:50].replace(" ",""))
    data_available = int(mb_response[45:50].replace(" ",""), 16)
    return data_available

out = ""

ret = read_input_reg(ADDR, 0x1000, 6, sleep_time=0.3)
# ret = read_input_reg(ADDR, 0x1000, 12, sleep_time=0.3)
if ret:
    out += "request :" + str(ret[0]) + "\n"
    out += "response:" + str(ret[1]) + "\n"

    if len(ret[1]) < 16:
        exit(0)

    data_items_count = get_msg_size(ret[1])
    out += "item count : " + str(data_items_count) + "\n"

    if(data_items_count > 10000):
        data_items_count = 0
    reqreg = 0

    if(1):
    # if(data_items_count):
        if(data_items_count > reqreg):
            reqreg = 9
        else:
            reqreg = data_items_count

        input_bytes = read_input_reg(ADDR, 0x2000, 125, 0.3)

        out += "data_items_count: " + str(data_items_count) + "\n"
        out += "request: " + str(input_bytes[0]) + "\n"
        out += "response :"+ str(input_bytes[1]) + "\n"
        out += "reqreg :"+ str(reqreg) + "\n"

        ret = write_multiple_regs(ADDR, 0x1000, (1,0,reqreg), sleep_time=0.1)
        # ret = write_multiple_regs(ADDR, 0x1000, (1,0,0xFFFF), sleep_time=0.1)
        if ret:
            if ret[0]:
                out += "approve :"+ str(ret[0]) + "\n"
            if ret[1]:
                out += "approve :"+ str(ret[1]) + "\n"

print(out)

time.sleep(0.1)
