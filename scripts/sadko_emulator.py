#!/usr/bin/python3
# -*- coding: iso-8859-15 -*-

import time
import serial
from modbus import *
import sys
import struct

if len(sys.argv) < 2:
    print("USAGE: %s [port] [baudrate]" % sys.argv[0])
    sys.exit(1)

set_port(sys.argv[1], sys.argv[2])

ADDR = 0xE

def get_msg_size(mb_response):
    # A1  A0  Q1  Q0  N   D (N байт)
    print("response:", mb_response[45:50].replace(" ",""))
    data_available = int(mb_response[45:50].replace(" ",""), 16)
    return data_available

out = ""

ret = read_input_reg(ADDR, 0x1000, 12, sleep_time=0.3)
if ret:
    out += "request :" + str(ret[0]) + "\n"
    out += "response:" + str(ret[1]) + "\n"

    if len(ret[1]) < 16:
        exit(0)

    data_items_count = get_msg_size(ret[1])
    out += "item count : " + str(data_items_count) + "\n"

    if(data_items_count > 10000):
        data_items_count = 0
    reqreg = 0

    print(out)
    timeset = [0x775B, 0x62B5, 0x0, 0x0]
    ret = write_multiple_regs(ADDR, 0x2008, timeset, sleep_time=0.3)

    print(ret)
    print("data_items_count " + str(data_items_count))

    while data_items_count:
        out = ""
        if(data_items_count > reqreg):
            reqreg = 9
        else:
            reqreg = data_items_count

        input_bytes = read_input_reg(ADDR, 0x2000, 125, 0.3)

        # out += "data_items_count: " + str(data_items_count) + "\n"
        out += "request: " + str(input_bytes[0]) + "\n"
        out += "response :"+ str(input_bytes[1]) + "\n"
        # out += "reqreg :"+ str(reqreg) + "\n"

        # approve

        print("reqreg " + str(reqreg))

        ret = write_multiple_regs(ADDR, 0x1000, (1,1,reqreg), sleep_time=0.3)
        print("APPROVE RET: " + str(ret))
        # ret = write_multiple_regs(ADDR, 0x1000, (1,0,0xFFFF), sleep_time=0.1)
        if ret:
            if ret[0]:
                out += "approve :"+ str(ret[0]) + "\n"
            if ret[1]:
                out += "approve :"+ str(ret[1]) + "\n"
        print(out)
        data_items_count -= reqreg
        if(data_items_count <= 1):
            break

        time.sleep(0.1)
