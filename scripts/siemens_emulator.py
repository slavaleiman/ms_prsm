#!/usr/bin/python3
# -*- coding: iso-8859-15 -*-

import time
import serial
from modbus import *
import sys
import struct

if len(sys.argv) < 3:
    print("USAGE: %s [port] [baudrate] [count]" % sys.argv[0])
    sys.exit(1)

ser = serial.Serial(sys.argv[1], sys.argv[2], timeout=3.0)

msgnum = int(sys.argv[3])
if not msgnum:
    msgnum = 1
num = 0

prime_numbers = [2, 3, 5, 7]
byte_array = bytearray(prime_numbers)

# output_string1 = [0xA, 0xA, 0xA, 0xA, 0xA]
output_string2 = [0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA]
# output_string3 = [0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA, 0xA]

def process_output_string(i):
    # 1: Закодированный номер участка (первый этап - 8..19; второй этап - 16..23, форсировка - 24..31, осадка - 32..40, проковка и подтяг - 41..)
    output_string2[0] = i & 0xFF
    # 2,3: Мгновенное положение подвижной колонны в 0,1 мм
    output_string2[1] = (i >> 8) & 0xFF
    output_string2[2] = i % 0xFF
    # 4,5: Мгновенный ток сварки в условных единицах аналогового входа (Ток (А) = (байт4*256+байт5 - 5529) * k / 22119) k=800, 1000 или 1500, предел установленного измерительного трансформатора тока
    output_string2[3] = (i >> 8) & 0xFF
    output_string2[4] = i % 0xFF
    # 6,7: Мгновенное напряжение на первичных обмотках в условных единицах аналогового входа (Напряжение (В) = (байт6*256+байт7 - 5529) * k / 22119) k=500 предел преобразователя напряжения
    output_string2[5] = (i >> 8) & 0xFF
    output_string2[6] = i % 0xFF
    # 8,9: Мгновенное давление в условных единицах аналогового входа (Давление (атм) = (байт6*256+байт7) * k / 27649) k - предел датчика давления, чаще всего 250
    output_string2[7] = (i >> 8) & 0xFF
    output_string2[8] = i % 0xFF
    # 10,11: Время от начала сварки в секундах (10 пакетов с одним и тем же значением каждую секунду сварки)
    output_string2[9] = (i >> 8) & 0xFF
    output_string2[10] = i % 0xFF

i = 0
for i in range(msgnum):
# while 1:
    if(ser.isOpen() == False):
        ser.open()
    i += 1
    process_output_string(i)

    num += 1
    print(str(num) + " -> " + str(output_string2))

    cmd_bytes = bytearray(output_string2)

    ser.write(cmd_bytes)
    time.sleep(0.03)
