#!/usr/bin/python3
# -*- coding: iso-8859-15 -*-

import time
import serial
from modbus import *
import sys
import struct

if len(sys.argv) < 2:
    print("USAGE: %s [port] [baudrate]" % sys.argv[0])
    sys.exit(1)

set_port(sys.argv[1], sys.argv[2])

ADDR = 0xE

out = ""

ret = read_input_reg(ADDR, 0x1000, 6, sleep_time=0.1)
if ret:
    out += "request :" + str(ret[0]) + "\n"
    out += "response:" + str(ret[1]) + "\n"
ret = read_input_reg(ADDR, 0x1000, 6, sleep_time=0.1)
if ret:
    out += "request :" + str(ret[0]) + "\n"
    out += "response:" + str(ret[1]) + "\n"

print(out)

time.sleep(0.1)
