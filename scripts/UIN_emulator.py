#!/usr/bin/python3
# -*- coding: iso-8859-15 -*-

import time
import serial
from modbus import *
import sys
import struct

msgnum = 1
if(len(sys.argv) > 3):
    msgnum = int(sys.argv[3])

ADDR = 0xA
uin_regs = [ADDR, 3,34,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,0xd,0xd] # last 2 bytes - pseudoCRC

def _read_holding_reg_(module_addr, register, num_regs=1, sleep_time=0.05):
    if(register & 0xFF00):
        cmd = "%02x 03 %02x %02x 00 %02x" % (int(module_addr), register >> 8, register & 0xFF, num_regs)
    else:
        cmd = "%02x 03 00 %02x 00 %02x" % (int(module_addr), register, num_regs)
    return cmd

if len(sys.argv) < 2:
    print("USAGE: %s [port] [baudrate] [num_messages]" % sys.argv[0])
    sys.exit(1)

ser = serial.Serial(sys.argv[1], sys.argv[2], timeout=3.0)
# set_port(sys.argv[1], sys.argv[2])

holding_bytes = _read_holding_reg_(ADDR, 0xA000, 17, 0.3)

time.sleep(0.1)

cmd_bytes = bytearray.fromhex(holding_bytes)

crc = calc(cmd_bytes)
crc_hi = crc >> 8
crc_lo = crc & 0xFF
cmd_bytes.append(crc_lo)
cmd_bytes.append(crc_hi)

for i in range(msgnum):

    ser.write(cmd_bytes)

    uin_regs[i % (len(uin_regs) - 4) + 4] = i % len(uin_regs)
    
    print(uin_regs)
    time.sleep(0.1)

    resp = bytearray(uin_regs)

    ser.write(bytearray(uin_regs))

    time.sleep(0.1)
